

import Foundation
import SwiftyJSON
import Alamofire
class Auth_API: NSObject {
    static let instance = Auth_API()
    let dev = UserDefaults.standard
    func login(param:[String:String],completion : @escaping(_ code: Int,_ status:Bool,_ result:JSON?) -> () ){
       
        Alamofire.request(loginURL, method: .post, parameters: param, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,false,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    print("status code : \(response.response?.statusCode ?? 0)")
                    if json["success"].boolValue{
                        let item = json["data"]["Member"]
                        UserDefaultsAPI.instance.setloginDefaults(item)
                    }else if json["data"]["active"].exists(){
                      self.dev.set("\(json["data"]["active"].intValue)", forKey: "id")
                    }
                    completion(response.response?.statusCode ?? 0,json["success"].boolValue,json)
                }
            default:
                print("get login code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,false,nil)
            }
        }
    }
    func register(param:[String:Any],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(registerURL, method: .post, parameters: param, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    if json["success"].boolValue{
                        UserDefaults.standard.setValue("\(json["data"]["memberId"].intValue)", forKey: "id")
                    }
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                print("get login code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    func activateAccount(param:[String:String],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(activateStudentAccountURL, method: .post, parameters: param, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    if json["success"].boolValue{
                        let item = json["data"]["Member"]
                        UserDefaultsAPI.instance.setloginDefaults(item)
                        self.dev.set(true, forKey: "is_login")
                    }
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                print("get activate account code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
    }
    func resendCode(phone:String,completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        let params:[String:Any] = ["phone":phone,
                                   "id":dev.string(forKey: "id")!
        ]
        Alamofire.request(resendCodeURL, method: .post, parameters: params, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                print("get resend code code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    func uploadPersonalImage(param:[String:Any],images:[String:UIImage],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key,image) in images{
                    multipartFormData.append(image.fixOrientation().jpegData(.medium)!, withName: "\(key)", fileName: "photo_\(key)_\(Int(Date().timeIntervalSince1970))"+".jpeg", mimeType: "image/jpeg")
                    print("image_\(key)_\(Int(Date().timeIntervalSince1970))"+".jpeg")
                }
                for (key, value) in param {
                    multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                }
        },
            to: uploadPersonalImageURL,
            method: .post,
            headers: Helper_API.instance.getHeaders()
            ,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.validate(statusCode: 200..<300)
                        .responseJSON { data in
                           
                            switch data.response?.statusCode{
                            case 200?:
                                let json = JSON(data.result.value!)
                                completion(data.response?.statusCode ?? 0,json)
                            default :
                                print("the status code sign up teacher \(data.response?.statusCode ?? 0)")
                                let responseJSON = try? JSON(data: data.data!)
                                print(responseJSON ?? 0)
                                print("you are failed")
                                completion(data.response?.statusCode ?? 0,nil)
                            }
                            
                    }
                case .failure(let encodingError):
                    print("the error is in default:\(encodingError)")
                    
                    completion(0,nil)
                    
                }
        }
        )
        
    }
    
    
      func editBasicInfo(param:[String:Any],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
          Alamofire.request(editBasicInfoURL, method: .post, parameters: param, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
              switch response.response?.statusCode {
              case 200?:
                  switch response.result{
                  case .failure( let error):
                      print(error)
                      completion(response.response?.statusCode ?? 0,nil)
                  case .success(let value):
                      let json = JSON(value)
                      print("basicccc \(json)")
                      completion(response.response?.statusCode ?? 0,json)
                  }
              default:
                  print("get basic info code  == \(response.response?.statusCode ?? 0)")
                  if let dat = response.data {
                      //print(dat)
                      let responseJSON = try? JSON(data: dat)
                      print(responseJSON)
                  }
                  completion(response.response?.statusCode ?? 0,nil)
                  
              }
          }
          
      }
}
