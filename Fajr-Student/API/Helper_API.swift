

import Foundation
import Alamofire
import SwiftyJSON
class Helper_API: NSObject {
    static let instance = Helper_API()
    let dev = UserDefaults.standard
    func getHeaders() -> HTTPHeaders {
         var header = HTTPHeaders()
         header["Accept"] = "application/json"
        guard let phone = dev.string(forKey: "phone"),let authKey = dev.string(forKey: "auth_key") else {return header}
         let auth = "\(phone):\(authKey)"
         header["Authorization"] = "Basic \(auth.toBase64() ?? "")"
         return header
     }
    func levels(completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        var url = ""
        if Bundle.main.preferredLocalizations[0] == "ar"{
        url = levelsURL + "ara"
        }else{
            url = levelsURL + "eng"
        }
        Alamofire.request(url, method: .get, parameters: nil, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                print("get levels code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    func subjectsUpOnLevel(id:String,completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        var url = ""
        if Bundle.main.preferredLocalizations[0] == "ar"{
            url = subjectsUpOnLevelURL + "\(id)/" + "ara"
        }else{
            url = subjectsUpOnLevelURL + "\(id)/" + "eng"
        }
        Alamofire.request(url, method: .post, parameters: nil, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                print("get subjects up on level code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
            }
        }
        
    }
    func postActionWithAuth(url:String,params:[String:Any],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(url, method: .post, parameters: params, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                print("get post action code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    
    func staticPages(id:String,completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
    
           Alamofire.request(staticPagesURL + id, method: .get, parameters: nil, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
               print("status code : \(response.response?.statusCode ?? 0)")
               switch response.response?.statusCode {
               case 200?:
                   switch response.result{
                   case .failure( let error):
                       print(error)
                       completion(response.response?.statusCode ?? 0,nil)
                   case .success(let value):
                       let json = JSON(value)
                       print(json)
                       completion(response.response?.statusCode ?? 0,json)
                   }
               default:
                   print("get static pages code  == \(response.response?.statusCode ?? 0)")
                   if let dat = response.data {
                       //print(dat)
                       let responseJSON = try? JSON(data: dat)
                       print(responseJSON)
                   }
                   completion(response.response?.statusCode ?? 0,nil)
                   
               }
           }
           
       }
    func notReadNoti(completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(notReadNotificationCountURL + "\(dev.string(forKey: "id") ?? "")", method: .get, parameters: nil, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                if let dat = response.data {
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    
    func transactions(completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(transactionsURL + (dev.string(forKey: "id") ?? "") + "/page:1", method: .post, parameters: nil, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
               switch response.response?.statusCode {
               case 200?:
                   switch response.result{
                   case .failure( let error):
                       print(error)
                       completion(response.response?.statusCode ?? 0,nil)
                   case .success(let value):
                       let json = JSON(value)
                       print(json)
                       completion(response.response?.statusCode ?? 0,json)
                   }
               default:
                   if let dat = response.data {
                       let responseJSON = try? JSON(data: dat)
                       print(responseJSON)
                   }
                   completion(response.response?.statusCode ?? 0,nil)
                   
               }
           }
           
       }
    
    func postAction(url:String,completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(url, method: .post, parameters: nil, encoding:URLEncoding.default , headers: nil).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                if let dat = response.data {
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    func addGroup(url:String,params:[String:Any],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(url, method: .post, parameters: params, encoding:JSONEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
               switch response.response?.statusCode {
               case 200?:
                   switch response.result{
                   case .failure( let error):
                       print(error)
                       completion(response.response?.statusCode ?? 0,nil)
                   case .success(let value):
                       let json = JSON(value)
                       print(json)
                       completion(response.response?.statusCode ?? 0,json)
                   }
               default:
                   if let dat = response.data {
                       let responseJSON = try? JSON(data: dat)
                       print(responseJSON)
                   }
                   completion(response.response?.statusCode ?? 0,nil)
                   
               }
           }
           
       }
    
    func getFunc(url:String,completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
     Alamofire.request(url, method: .post, parameters: nil, encoding:JSONEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
         print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                if let dat = response.data {
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    
    func payment(url:String,params:[String:Any],completion : @escaping(_ code: Int,_ result:JSON?) -> () ){
        Alamofire.request(url, method: .post, parameters: params, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 201?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    completion(response.response?.statusCode ?? 0,json)
                }
            default:
                if let dat = response.data {
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,nil)
                
            }
        }
        
    }
    func postActionWithAuthAwad(url:String,params:[String:Any],completion : @escaping(_ code: Int,_ success:Bool,_ reserv:ReservationModel?,_ students:[StudentModel]?) -> () ){
        Alamofire.request(url, method: .post, parameters: params, encoding:URLEncoding.default , headers: Helper_API.instance.getHeaders()).validate(statusCode: 200..<300).responseJSON { (response) in
            print("status code : \(response.response?.statusCode ?? 0)")
            switch response.response?.statusCode {
            case 200?:
                switch response.result{
                case .failure( let error):
                    print(error)
                    completion(response.response?.statusCode ?? 0,false,nil,nil)
                case .success(let value):
                    let json = JSON(value)
                    print(json)
                    let dc = JSONDecoder()
                    var students = [StudentModel]()
                    var reserv:ReservationModel?
                    for item in json["data"]["students"].arrayValue{
                      do{
                          let js = try item["Student"].rawData()
                          let vv = try dc.decode(StudentModel.self, from: js)
                          students.append(vv)
                    }catch{
                        print(error.localizedDescription)
                    }
                    }
                    do{
                        let js = try json["data"]["Reservation"].rawData()
                        reserv = try dc.decode(ReservationModel.self, from: js)
                    }catch{
                        print(error.localizedDescription)
                    }
                    completion(response.response?.statusCode ?? 0,json["success"].boolValue,reserv,students)
                default:
                    completion(response.response?.statusCode ?? 0,false, nil, nil)
                }
            default:
                print("get post action code  == \(response.response?.statusCode ?? 0)")
                if let dat = response.data {
                    //print(dat)
                    let responseJSON = try? JSON(data: dat)
                    print(responseJSON)
                }
                completion(response.response?.statusCode ?? 0,false,nil,nil)
                
            }
        }
        
    }
}
