


import Foundation
// MARK: - Levels
struct Levels: Codable {
    var go: Go

    enum CodingKeys: String, CodingKey {
        case go = "go"
    }
}

// MARK: - Go
struct Go: Codable {
    var id: String
    var name: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}

