
import Foundation
import SwiftyJSON
class UserDefaultsAPI: NSObject {
    static let instance = UserDefaultsAPI()
    let dev = UserDefaults.standard
    func setloginDefaults(_ item:JSON){
        self.dev.set(item["id"].stringValue, forKey: "id")
        self.dev.set(item["f_name"].stringValue, forKey: "f_name")
        self.dev.set(item["l_name"].stringValue, forKey: "l_name")
        self.dev.set(item["photo"].stringValue, forKey: "photo")
        self.dev.set(item["is_parent"].boolValue, forKey: "is_parent")
        self.dev.set(item["email"].stringValue, forKey: "email")
        self.dev.set(item["phone"].stringValue, forKey: "phone")
        self.dev.set(item["active"].boolValue, forKey: "active")
        self.dev.set(item["students_teachers"].stringValue, forKey: "students_teachers")
        self.dev.set(item["student_level_id"].stringValue, forKey: "student_level_id")
        self.dev.set(item["student_level_name_ara"].stringValue, forKey: "student_level_name_ara")
        self.dev.set(item["student_level_name_eng"].stringValue, forKey: "student_level_name_eng")
        self.dev.set(item["gender"].stringValue, forKey: "gender")
        self.dev.set(item["profile_ratio"].stringValue, forKey: "profile_ratio")
        self.dev.set(item["auth_key"].stringValue, forKey: "auth_key")
        self.dev.set(item["available"].boolValue, forKey: "available")
    }
}
