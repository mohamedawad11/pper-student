//
//  AppDelegate.swift
//  Fajr-Student
//
//  Created by osx on 12/23/20.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import FirebaseAuth
import Firebase
import FirebaseMessaging
import SwiftyJSON
//import MyFawryPlugin

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //AIzaSyDk1KBFyKnheZYe6cgG0ljsha59S1eBFFw
        GMSPlacesClient.provideAPIKey(bigKey)
        GMSServices.provideAPIKey(bigKey)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        FirebaseApp.configure()
        setupNotifications(application)
        return true
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        print("connnection done")
        SocketIOManager.sharedInstance.establishConnection()
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        setSocketWithLeaveStudentroom()
        SocketIOManager.sharedInstance.closeConnection()
    }
    func setSocketWithLeaveStudentroom(){
        if UserDefaults.standard.bool(forKey: "is_login"),UserDefaults.standard.string(forKey: "id") != nil{
            SocketIOManager.sharedInstance.socket.emitWithAck("leave_student_room", with: [UserDefaults.standard.string(forKey: "id")!]).timingOut(after: 4) { (data) in
            }
        }
    }
    
    func setupNotifications(_ application: UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert,.sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        
    }
    
}


extension AppDelegate :  MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.set(fcmToken, forKey: "FCMToken")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("APNs device token: \(deviceTokenString)")
        
    }
    
}

extension AppDelegate : UNUserNotificationCenterDelegate{
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let xx = userInfo as? [String:Any] {
            print("foreNoti$$$",JSON(xx))
        }
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let xx = userInfo as? [String:Any] {
            print("BackNoti$$$",xx)
            let jsonData = JSON(xx)
            
            switch jsonData["type"].stringValue {
            case "group":
                notificationType = 1
                notificationJS = jsonData
                let userInfo:[String:Any] = ["id":jsonData["group_id"].stringValue]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "groupNoti"), object: nil, userInfo: userInfo)
            case "reservation":
                notificationType = 2
                notificationJS = jsonData
                let userInfo:[String:Any] = ["id":jsonData["reservation_id"].stringValue]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reservationNoti"), object: nil, userInfo: userInfo)
            default:
                print("no thing")
            }
            
        }
        completionHandler()
    }
    
}


