//
//  ForgetPaaFirstStepVC.swift
//  Fajr-Student
//
//  Created by osx on 1/13/21.
//
import UIKit
import VKPinCodeView
class ForgetPaaFirstStepVC: UIViewController {
    @IBOutlet weak var code_TF: UITextField!
    @IBOutlet weak var back_b: UIButton!
    
    @IBOutlet weak var pinView: VKPinCodeView!
    
    var code = ""
    var phone = ""
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if self.is_ar() == "ar"{
        back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }
    @IBAction func resendCode_btn(_ sender: Any) {
        BGLoading.instance.showLoading(view)
        let par = ["phone":phone,"students_teachers":"student"]
        Helper_API.instance.postActionWithAuth(url: resetPassFirstStepURL, params: par) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200{
                if let item = result{
                    if item["success"].boolValue{
                        self.toast("Rsend Code Successfully")
                    }else{
                    self.to_ast(item["data"].stringValue)
                    }
                }
            }else{
                self.toast("internet")
            }
        }
    }
    
    @IBAction func confirm_tbn(_ sender: Any) {
        if code_TF.text ?? "" == self.code {
            let vc = UIStoryboard(name: "ForgetPassword", bundle: nil).instantiateViewController(withIdentifier: "ForgetPassSecondStepVC") as! ForgetPassSecondStepVC
            vc.phone = self.phone
            self.navigationController?.pushViewController(vc, animated: false)
        }else{
            self.toast("The code is incorrect")
        }
    
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

