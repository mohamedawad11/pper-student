//
//  ForgetPassSecondStepVC.swift
//  Fajr-Student
//
//  Created by osx on 1/13/21.
//

import UIKit

class ForgetPassSecondStepVC: UIViewController {
    
    @IBOutlet weak var back_b: UIButton!
    @IBOutlet weak var confirmPass_tf: UITextField!
    @IBOutlet weak var pass_tf: UITextField!
    var phone = ""
    let dev = UserDefaults.standard
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if self.is_ar() == "ar"{
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func confirm_btn(_ sender: UIButton) {
        guard self.checkPassword(pass_tf, confirmPass_tf) else {
            return
        }
        let params = [
            "id":UserDefaults.standard.string(forKey: "id")!,
            "password":pass_tf.text!
        ]
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: resetPassSecondStepURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            sender.isUserInteractionEnabled = true
            if code == 200 ,let item = result{
                  
                    if item["success"].boolValue{
                          self.toast("Reset Password Successfully")
                        UserDefaults.standard.set(true, forKey: "is_login")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: (self.dev.bool(forKey: "is_parent") ? "homeParentID" : "homeID"))
                            self.present(vc, animated: false, completion: nil)
                        }
                        
                }else{
                    self.toast("internet")
                }
            }else{
                self.toast("internet")
            }
        }
        
    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
