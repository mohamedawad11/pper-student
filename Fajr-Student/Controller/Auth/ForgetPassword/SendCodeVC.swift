//
//  SendCodeVC.swift
//  Fajr-Student
//
//  Created by osx on 1/13/21.
//

import UIKit

class SendCodeVC: UIViewController {
    @IBOutlet weak var phoneCodeLB: UILabel!

    @IBOutlet weak var back_b: UIButton!
    
    @IBOutlet weak var phone_tf: UITextField!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        phone_tf.placeholder = phonePlaceHolder
        phoneCodeLB.text = countryFlag + CountryCode
        if self.is_ar() == "ar"{
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
    }
    
    @IBAction func send_btn(_ sender: Any) {
        guard !phone_tf.text!.isEmpty else {
            self.toast("Enter Phone Number")
            return
        }
        var PhoneString = phone_tf.text ?? ""
        if !(PhoneString.convertToEnglish()).hasPrefix("0"){
            PhoneString = CountrySymbol == "EG" ? "0\(PhoneString)" : PhoneString
        }
        
        let vCo = (CcodePhone).hasPrefix("+") ? String(CcodePhone.dropFirst()) : CcodePhone
        let params:[String:String] = [
            "phone":"\(vCo)\(PhoneString.convertToEnglish())","students_teachers":"student"
        ]
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: resetPassFirstStepURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200 ,let data = result{
                 if data["success"].boolValue{
                    UserDefaultsAPI.instance.setloginDefaults(data["data"]["Member"])
                     let VerificationCode = data["data"]["Member"]["virification_code"].stringValue
                     let vc = UIStoryboard(name: "ForgetPassword", bundle: nil).instantiateViewController(withIdentifier: "ForgetPaaFirstStepVC") as! ForgetPaaFirstStepVC
                     vc.code = VerificationCode
                     vc.phone = "\(vCo)\(PhoneString.convertToEnglish())"
                     self.navigationController?.pushViewController(vc, animated: false)
                 }else{
                     self.to_ast(data["data"].stringValue)
                 }
             }else{
                 self.toast("internet")
             }
        }
    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
