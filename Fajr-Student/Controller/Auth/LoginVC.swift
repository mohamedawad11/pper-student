

import UIKit


class LoginVC: UIViewController {
    @IBOutlet weak var phoneCodeLB: UILabel!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    let dev = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTF.placeholder = phonePlaceHolder
        phoneCodeLB.text = countryFlag + CountryCode
    }
    private func validateData() -> Bool {
        guard self.checkValidPhone(phoneTF) else{return false}
        guard !(passwordTF.text!.isEmpty) else{
            self.toast("Enter Password")
            return false}
        return true
    }
    @IBAction func loginAction(_ sender: UIButton){
        guard validateData() else{
            return
        }
        sender.isUserInteractionEnabled = false
        var PhoneString = phoneTF.text ?? ""
        if !(PhoneString.convertToEnglish()).hasPrefix("0"){
            PhoneString = CountrySymbol == "EG" ? "0\(PhoneString)" : PhoneString
        }
        
        let vCo = (CcodePhone).hasPrefix("+") ? String(CcodePhone.dropFirst()) : CcodePhone
        let parm = ["phone":"\(vCo)\(PhoneString.convertToEnglish())","password":passwordTF.text!,"device_token":UserDefaults.standard.string(forKey: "FCMToken") ?? ""]
        print("login params \n \(parm)")
        BGLoading.instance.showLoading(view)
        Auth_API.instance.login(param: parm) { (code, status,result) in
            BGLoading.instance.dismissLoading()
            sender.isUserInteractionEnabled = true
            if code == 200 {
                if let item = result{
                    
                    if item["success"].boolValue{
                        UserDefaults.standard.setValue(true, forKey: "is_login")
                            let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: (self.dev.bool(forKey: "is_parent") ? "homeParentID" : "homeID"))
                        
                            self.present(vc, animated: false, completion: nil)
                    }else if item["data"]["active"].exists(){
                        self.to_ast(item["data"]["message"].stringValue)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let vc = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeVC") as! VerificationCodeVC
                            vc.phone = "\(vCo)\(PhoneString.convertToEnglish())"
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }else{
                        self.to_ast(item["data"]["message"].stringValue)
                    }
                }
            }else if code == 0{
                self.toast("internet")
            }else{
                self.toast("internet")
            }
        }
        
    }
    @IBAction func showHidePAss_btn(_ sender: UIButton) {
        if passwordTF.isSecureTextEntry {
            passwordTF.isSecureTextEntry = false
            sender.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        }else{
            passwordTF.isSecureTextEntry = true
            sender.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
        }
    }
    
    @IBAction func forgetPass_btn(_ sender: Any) {
        self.navigationController?.pushViewController(UIStoryboard(name: "ForgetPassword", bundle: nil).instantiateViewController(withIdentifier: "SendCodeVC"), animated: false)
    }
    @IBAction func joinUs_btn(_ sender: Any) {
        self.navigationController?.pushViewController(UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "RegisterVC"), animated: false)
        
    }
    @IBAction func signUpTeacher_btn(_ sender: Any) {
        let instagramHooks = "PPerTeacherApp://"
        let instagramUrl = URL(string: instagramHooks)!
        if UIApplication.shared.canOpenURL(instagramUrl)
        {
            UIApplication.shared.open(instagramUrl)
        } else {
            self.rateApp(appId: "id1549472942")
        }
        
    }
    
    @IBAction func skip_btn(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "is_login")
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "homeParentID")
        self.present(vc, animated: false, completion: nil)
    }
}

