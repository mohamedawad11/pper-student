
import UIKit
import SwiftyJSON
class RegisterVC: UIViewController {
    @IBOutlet weak var phoneCodeLB: UILabel!

    @IBOutlet weak var student_img: UIImageView!
    @IBOutlet weak var parent_img: UIImageView!
    
    
    
    @IBOutlet weak var levelB: UIButton!
    @IBOutlet weak var levelStackView: UIStackView!
    @IBOutlet weak var back_b: UIButton!
    @IBOutlet weak var f_nameTF: UITextField!
    
    @IBOutlet weak var l_nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var parent_b: UIButton!
    @IBOutlet weak var student_b: UIButton!
    @IBOutlet weak var password_Tf: UITextField!
    @IBOutlet weak var confirm_PasswordTF: UITextField!
    var student_level_id = "-1"
    var levels = [JSON]()
    var levels_names = [String]()
    var is_parent = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTF.placeholder = phonePlaceHolder
        phoneCodeLB.text = countryFlag + CountryCode
        levelB.contentHorizontalAlignment = .left
        if self.is_ar() == "ar"{
            levelB.contentHorizontalAlignment = .right
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
        getLevels()
        
    }
    
    @IBAction func typeAction_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            student_img.tintColor = UIColor(named: "OrColor")
            student_b.setTitleColor(UIColor(named: "OrColor"), for: .normal)
            parent_b.tintColor = .lightGray
            parent_img.tintColor = .lightGray
            is_parent = 0
            student_level_id = "-1"
            levelB.setTitle("Choose level".localized(), for: .normal)
            parent_b.setTitleColor(.lightGray, for: .normal)
            
            UIView.animate(withDuration: 0.2) {
                self.levelB.setTitleColor(.lightGray, for: .normal)
                self.levelStackView.isHidden = false
            }
        case 1:
            parent_img.tintColor = UIColor(named: "OrColor")
            parent_b.setTitleColor(UIColor(named: "OrColor"), for: .normal)
            
            student_b.tintColor = .lightGray
            student_img.tintColor = .lightGray
            student_b.setTitleColor(.lightGray, for: .normal)
            is_parent = 1
            student_level_id = "0"
            UIView.animate(withDuration: 0.2) {
                self.levelStackView.isHidden = true
            }
        default:
            print("no thing")
        }
    }
    @IBAction func showHide_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            if password_Tf.isSecureTextEntry{
                password_Tf.isSecureTextEntry = false
                sender.setImage(UIImage(systemName: "eye.fill"), for: .normal)
            }else{
                password_Tf.isSecureTextEntry = true
                sender.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
            }
        case 1:
            if confirm_PasswordTF.isSecureTextEntry{
                confirm_PasswordTF.isSecureTextEntry = false
                sender.setImage(UIImage(systemName: "eye.fill"), for: .normal)
                
            }else{
                confirm_PasswordTF.isSecureTextEntry = true
                sender.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
            }
        default:
            print("no thing")
        }
    }
    
    func getLevels(){
        Helper_API.instance.levels { (code, result) in
            if code == 200,let data = result {
                if data["success"].boolValue{
                    self.levels = data["data"].arrayValue
                    for item in data["data"].arrayValue{
                        self.levels_names.append(item["name"].stringValue)
                    }
                }else{
                    self.to_ast(data["data"]["message"].stringValue)
                }
            }else{
                self.toast("internet")
            }
        }
    }
    @IBAction func level_btn(_ sender: UIButton) {
        self.drop(anchor: sender, dataSource: levels_names) { (item, index) in
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(.black, for: .normal)
            self.student_level_id = self.levels[index]["id"].stringValue
        }
    }
    func validateData()->Bool{
        guard !f_nameTF.text!.isEmpty else {
            self.toast("Enter First Name")
            return false
        }
        guard !l_nameTF.text!.isEmpty else {
            self.toast("Enter Last Name")
            return false
        }
        guard !emailTF.text!.isEmpty else {
            self.toast("Enter Email Address")
            return false
        }
        guard Validaton.isValidEmailAddress(emailAddressString: emailTF.text!) else {
            self.toast("Enter Valid Email Address")
            return false
        }
        guard self.checkValidPhone(phoneTF) else {
            return false
        }
        
        guard student_level_id != "-1" else {
            self.toast("Choose Student Level")
            return false
        }
        guard self.checkPassword(password_Tf, confirm_PasswordTF) else {
            return false
        }
        
        return true
    }
    @IBAction func continue_btn(_ sender: Any) {
        guard validateData() else {
            return
        }
        var PhoneString = phoneTF.text ?? ""
        if !(PhoneString.convertToEnglish()).hasPrefix("0"){
            PhoneString = CountrySymbol == "EG" ? "0\(PhoneString)" : PhoneString
        }
        
        let vCo = (CcodePhone).hasPrefix("+") ? String(CcodePhone.dropFirst()) : CcodePhone
        let params:[String:Any] = [
            "fName":f_nameTF.text!,
            "lName":l_nameTF.text!,
            "phone":"\(vCo)\(PhoneString.convertToEnglish())",
            "email":emailTF.text!,
            "password":password_Tf.text!,
            "is_parent":is_parent,
            "student_level_id":student_level_id,
            "deviceToken":UserDefaults.standard.string(forKey: "FCMToken") ?? ""
        ]
        print("sign up params :\n \(params)")
        BGLoading.instance.showLoading(view)
        Auth_API.instance.register(param: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200 ,let data = result{
                if data["success"].boolValue{
                    self.to_ast(data["data"]["message"].stringValue)
                    let vc = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeVC") as! VerificationCodeVC
                    vc.phone = "\(vCo)\(PhoneString.convertToEnglish())"
                    self.navigationController?.pushViewController(vc, animated: false)
//                    BGLoading.instance.showLoading(self.view)
//                    Auth_API.instance.resendCode(phone: self.phoneTF.text!.convertToEnglish()) { (code, result) in
//                        BGLoading.instance.dismissLoading()
//                        if code == 200{
//                            if let item = result,item["success"].boolValue{
//                                self.to_ast(item["data"]["message"].stringValue)
//                                    let vc = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "VerificationCodeVC") as! VerificationCodeVC
//                                vc.phone = "\(CcodePhone)\((self.phoneTF.text ?? "").convertToEnglish())"
//                                    self.navigationController?.pushViewController(vc, animated: false)
//                            }
//                        }else{
//                            self.toast("internet")
//                        }
//                    }
                    
                }else{
                    self.to_ast(data["data"]["message"].stringValue)
                }
            }else{
                self.toast("internet")
            }
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
