//
//  SignUpCompletedVC.swift
//  Fajr-Student
//
//  Created by osx on 12/24/20.
//

import UIKit

class SignUpCompletedVC: UIViewController {

    var parm : [String:String]?
    let dev = UserDefaults.standard
    @IBOutlet weak var txt_lb: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        txt_lb.setLineHeight(lineHeight: 1.5)
    }
 
    @IBAction func complete(){
       self.present(UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: (self.dev.bool(forKey: "is_parent") ? "homeParentID" : "homeID")), animated: false, completion: nil)
    }
}

