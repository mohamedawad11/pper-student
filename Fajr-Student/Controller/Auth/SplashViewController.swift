

import UIKit

class SplashViewController: UIViewController {
    var window: UIWindow?
    let dev = UserDefaults.standard
      override func viewDidLoad() {
          super.viewDidLoad()
          _ = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(navigateToLogin), userInfo: nil, repeats: false)
        if self.is_login(){
            let params = [
                "member_id":UserDefaults.standard.string(forKey: "id") ?? "0",
                "lang":self.is_ar() == "ar" ? "ara" : "eng"
            ]
            Helper_API.instance.postActionWithAuth(url: setusedLanguage, params: params) { (code, result) in
                if code == 200{
                    print("success to set lang")
                }else{
                    print("fail to set lang")
                }
            }
        }
      }

      @objc func navigateToLogin(){
          let status = UserDefaults.standard.bool(forKey: "is_login")
          if status {
              let storyBoard : UIStoryboard = UIStoryboard(name: "Home", bundle:nil)
              let newViewController = storyBoard.instantiateViewController(withIdentifier: (self.dev.bool(forKey: "is_parent") ? "homeParentID" : "homeID"))
         self.window = UIWindow(frame: UIScreen.main.bounds)
              self.window?.rootViewController = newViewController
              self.window?.makeKeyAndVisible()
          }else{
              DispatchQueue.main.async {
                  let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
                  let newViewController = storyBoard.instantiateViewController(withIdentifier: "authRoot")
               self.window = UIWindow(frame: UIScreen.main.bounds)
                       self.window?.rootViewController = newViewController
                       self.window?.makeKeyAndVisible()
              }
          }
          
      }
  }

