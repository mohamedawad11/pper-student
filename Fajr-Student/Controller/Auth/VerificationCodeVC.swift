
import UIKit
import VKPinCodeView
class VerificationCodeVC: UIViewController {
    @IBOutlet weak var back_b: UIButton!
    @IBOutlet weak var code_TF: UITextField!
    var code = ""
    var userID = ""
    var phone = ""
    override func viewWillAppear(_ animated: Bool) {
        if self.is_ar() == "ar"{
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func resendCode_btn(_ sender: Any) {
        BGLoading.instance.showLoading(view)
        Auth_API.instance.resendCode(phone: phone) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200{
                if let item = result,item["success"].boolValue{
                    self.to_ast(item["data"]["message"].stringValue)
                }
            }else{
                self.toast("internet")
            }
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func confirmBTN(_ sender: UIButton) {
        guard !code_TF.text!.isEmpty else {
            return
        }
        let params = ["virification_code" : code_TF.text ?? "", "phone":self.phone]
        print("params\(params)")
        BGLoading.instance.showLoading(self.view)
        Auth_API.instance.activateAccount(param: params) { (code,result) in
            BGLoading.instance.dismissLoading()
            if code == 200,let item = result,item["success"].boolValue {
                    self.toast("Success!")
                        self.present(UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "SignUpCompletedVC") as! SignUpCompletedVC, animated: false, completion: nil)
            }else if code == 0{
                self.toast("internet")
            }else{
                if let item = result{
                    self.to_ast(item["data"]["message"].stringValue)
                }
            }
        }
    }
    
}

