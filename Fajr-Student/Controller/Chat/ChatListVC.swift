

import UIKit
import SwiftyJSON
class ChatListVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backB: UIButton!
    let dev = UserDefaults.standard
    var chats = [JSON]()
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.register(UINib(nibName: "chatRoomCell", bundle: nil), forCellReuseIdentifier: "chatRoomCell")
        tableView.delegate = self
        tableView.dataSource = self
        getchats()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        
    }
    func getchats(){
        let par = ["student_id":dev.string(forKey: "id") ?? ""]
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: chatListURL, params: par) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200,let data = result,data["success"].boolValue{
                self.tableView.isHidden = false
                self.chats = data["data"].arrayValue
                guard self.chats.count > 0 else {
                    self.tableView.isHidden = true
                    return
                }
                self.tableView.reloadData()
            }else if code == 401{                
                self.sessionExpired()
            } else{
                self.tableView.isHidden = true
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chatRoomCell", for: indexPath) as? chatRoomCell,chats.count > 0 else { return UITableViewCell()}
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let pos = indexPath.row
        let itemCollection = chats[pos]["Collection"]
        let itemTeacher = chats[pos]["Teacher"]
        let fname = itemTeacher["f_name"].stringValue
        let mname = itemTeacher["m_name"].stringValue
        let lname = itemTeacher["l_name"].stringValue
        cell.teacherNameLB.text = "\(fname) \(mname) \(lname)"
        cell.courseNameLB.text = itemCollection["collection_name"].stringValue
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pos = indexPath.row
        let itemCollection = chats[pos]["Collection"]
        let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.group = itemCollection
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func backBTn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func reloadData_btn(_ sender: Any) {
        getchats()
    }
    
}
