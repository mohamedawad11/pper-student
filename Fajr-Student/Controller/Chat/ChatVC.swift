

import UIKit
import SwiftyJSON
import SocketIO
class ChatVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var send_b: UIButton!
    @IBOutlet weak var back_b: UIButton!
    @IBOutlet weak var groupName_lb: UILabel!
    @IBOutlet weak var message_tf: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var group:JSON!
    let dev = UserDefaults.standard
    var msgs = [JSON]()
    var dateFormate = DateFormatter()
    override func viewWillAppear(_ animated: Bool) {
        groupName_lb.text = group["collection_name"].stringValue
        SocketIOManager.sharedInstance.socket.emitWithAck("join_chat_room", with: [group["id"].stringValue,UserDefaults.standard.string(forKey: "id")!,false,"\(dev.string(forKey: "f_name")!) \(dev.string(forKey: "l_name")!)",dev.string(forKey: "FCMToken")!]).timingOut(after: 4) { (data) in
        }
        SocketIOManager.sharedInstance.socket.on("old_messages") { (data, ack) in
            print("oldmessss \(JSON(data[0]))")
            if JSON(data[0]).arrayValue.count > 0 {
                self.msgs.append(contentsOf: JSON(data[0]).arrayValue)
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.msgs.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
        SocketIOManager.sharedInstance.socket.on("new_order_chat_message") { (data, ack) in
            print("cvccnvbncvnb\(JSON(data))")
            if JSON(data).arrayValue.count > 0 {
                print("ggggghhhh")
                self.msgs.append(contentsOf: JSON(data).arrayValue)
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.msgs.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        SocketIOManager.sharedInstance.socket.emitWithAck("leave_chat_room", with: [group["id"].stringValue,UserDefaults.standard.string(forKey: "id")!]).timingOut(after: 4) { (data) in
               }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let pos = indexPath.row
        let item = msgs[pos]
        if item["user_id"].stringValue == UserDefaults.standard.string(forKey: "id")!{
            registerNib("ChatToCell")
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatToCell", for: indexPath) as? ChatToCell,msgs.count > 0 {
                cell.selectionStyle = UITableViewCell.SelectionStyle.none
                cell.message_lb.text = item["message"].stringValue
                cell.date_lb.text = self.getDateFromString("dd/MM/yyyy hh:mm", item["message_date"].stringValue, "hh:mm a")
                return cell
            }else{
                return UITableViewCell()
            }
        }else{
            registerNib("ChatFromCells")
                   if let cell = tableView.dequeueReusableCell(withIdentifier: "ChatFromCells", for: indexPath) as? ChatFromCells,msgs.count > 0 {
                       cell.selectionStyle = UITableViewCell.SelectionStyle.none
                       cell.message_lb.text = item["message"].stringValue
                    cell.username_lb.text = item["user_name"].stringValue
                    cell.date_lb.text = self.getDateFromString("dd/MM/yyyy hh:mm", item["message_date"].stringValue, "hh:mm a")

                       return cell
                   }else{
                       return UITableViewCell()
                   }
        }
    }
    func registerNib(_ identifier:String){
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
    @IBAction func send_btn(_ sender: UIButton) {
        guard !message_tf.text!.isEmpty else {
            return
        }
        sender.isUserInteractionEnabled = false
        dateFormate.locale = Locale(identifier: "en_US")
        dateFormate.dateFormat = "dd/MM/yyyy hh:mm"
        SocketIOManager.sharedInstance.socket.emitWithAck("send_chat_message", group["id"].stringValue,dev.string(forKey: "id")!,"\(dev.string(forKey: "f_name")!) \(dev.string(forKey: "l_name")!)",message_tf.text!,dateFormate.string(from: Date()),dev.string(forKey: "photo")!,group["collection_name"].stringValue).timingOut(after: 4) { (data) in
            self.message_tf.text?.removeAll()
            sender.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
