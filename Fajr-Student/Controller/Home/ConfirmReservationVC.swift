
import UIKit
import SwiftyJSON
class ConfirmReservationVC: UIViewController {
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var teacherNameLB: UILabel!
    @IBOutlet weak var subjectLB: UILabel!
    @IBOutlet weak var reDateLB: UILabel!
    @IBOutlet weak var reClockLB: UILabel!
    @IBOutlet weak var numOfStudentLB: UILabel!
    @IBOutlet weak var priceLB: UILabel!
    var techProfile:TeacherProfileModel?
    var params = [String:Any]()
    var timeJson:JSON!
    var price = ""
    let dev = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        if let item = techProfile{
            teacherNameLB.text = "\(item.fName ?? "") \(item.mName ?? "") \(item.lName ?? "")"
            subjectLB.text =  params["subjectName"] as? String ?? ""
            reDateLB.text = params["schedule_day"] as? String ?? ""
            reClockLB.text = timeJson["from"].stringValue + "-" + timeJson["to"].stringValue
            numOfStudentLB.text = "\(params["max_num"] as? String ?? "") \("Student".localized())"
            priceLB.text = "\((item.costPerStudent! as NSString).doubleValue * (params["max_num"]! as! String as NSString).doubleValue) \(currencyTXT)"
            price = "\((item.costPerStudent! as NSString).doubleValue * (params["max_num"]! as! String as NSString).doubleValue)"
            if let photo = item.photo,let teachID = item.id{
                            logo.sd_setImage(with: URL(string: "\(imageURL)\(teachID)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
                        }else{
                           logo.image = UIImage(named: "profilePlaceHolder")
                        }
        }
        
    }
    @IBAction func confirmBTN(_ sender: Any) {
        let param:[String:Any] = [
            "teacher_id":techProfile!.id!,
            "schedule_day":params["schedule_day"] as? String ?? "",
            "schedule_from":timeJson["from"].stringValue,
            "schedule_to":timeJson["to"].stringValue,
            "collectionId":params["collectionId"] as? String ?? "",
            "teacher_subject_id":techProfile!.subjectID!,
            "price":price,
            "lat":techProfile!.lat!,
            "lon":techProfile!.lon!,
            "student_id":dev.string(forKey: "id") ?? "",
            "children_num":params["max_num"] as? String ?? ""
        ]
        print("params \(param)")
        BGLoading.instance.showLoading(view)
        let lang = self.is_ar() == "ar" ? "ara":"eng"
        
        Helper_API.instance.postActionWithAuth(url: insertReservationAndInvoiceURL + lang , params: param) { (code, result) in
           BGLoading.instance.dismissLoading()
            if code == 200,let data = result{
                if data["success"].boolValue{
                    self.to_ast(data["message"].stringValue)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        ishaveReservation = true
                        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: (self.dev.bool(forKey: "is_parent") ? "homeParentID" : "homeID"))
                        self.present(vc, animated: false, completion: nil)
                    }
                }else{
                    self.toast("Session Finished , Sign In Please")
                }
            }else{
                self.toast("internet")
            }
        }
    }
    
    
    
    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
