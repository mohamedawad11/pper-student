

import UIKit
import SwiftyJSON
class CourseTimeVC: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource{

    @IBOutlet weak var hideTimesB: UIButton!
    @IBOutlet weak var timesView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var courseTimeLB: UILabel!
    @IBOutlet weak var totalPriceLB: UILabel!
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var subjectLB: UILabel!
    @IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var clockLB: UILabel!
    @IBOutlet weak var teacherNameLB: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    var profile:TeacherProfileModel?
    var params = [String:Any]()
    var times = [JSON]()
    var timeJson:JSON?
    var selectedTime = "-1"
    override func viewWillAppear(_ animated: Bool) {
        hideTimesB.isHidden = true
        timesView.isHidden = true
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        if let item = profile{
                  if let photo = item.photo,let teachID = item.id{
                      logoImg.sd_setImage(with: URL(string: "\(imageURL)\(teachID)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
                  }else{
                     logoImg.image = UIImage(named: "profilePlaceHolder")
                  }
                  teacherNameLB.text = "\(item.fName ?? "") \(item.mName ?? "") \(item.lName ?? "")"
                  priceLB.text = "\(item.costPerStudent ?? "") \(currencyTXT)"
                  clockLB.text = "\(item.deurationPerHoures ?? "") \("Hours".localized())"
                  subjectLB.text = params["subjectName"] as? String ?? ""
           
            totalPriceLB.text = "\((item.costPerStudent! as NSString).doubleValue * (params["max_num"]! as! String as NSString).doubleValue) \(currencyTXT)"
              }
        collectionView.register(UINib(nibName: "DayDetailsCell", bundle: nil), forCellWithReuseIdentifier: "DayDetailsCell")
        collectionView.delegate = self
        collectionView.dataSource = self
     getDayDetails()
    }
    func getDayDetails(){
        let par:[String:Any] = [
            "schedule_day":params["schedule_day"] as? String ?? "",
            "teacher_id":profile!.id ?? "",
            "courseDurationPerHour":((profile!.deurationPerHoures ?? "") as NSString).integerValue * 60
        ]
        Helper_API.instance.postActionWithAuth(url: dayDetailsURL, params: par) { (code, result) in
            if code == 200,let data = result{
                if data["success"].boolValue{
                    self.times = data["data"].arrayValue
                    self.collectionView.reloadData()
                }else{
                    self.to_ast(data["message"].stringValue)
                }
            }else{
                self.toast("internet")
            }
        }
    
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return times.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DayDetailsCell", for: indexPath) as? DayDetailsCell else{
            return UICollectionViewCell()
        }
        let pos = indexPath.row
        let item = times[pos]
        cell.nameLB.text = item["from"].stringValue + "-" + item["to"].stringValue
        if item["reserved"].boolValue{
            cell.bgv.borderColor = #colorLiteral(red: 1, green: 0.8859711938, blue: 0.9362788099, alpha: 1)
        }else{
            cell.bgv.borderColor = UIColor(named: "OrColor")!
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pos = indexPath.row
        let item = times[pos]
        if !item["reserved"].boolValue{
            selectedTime = item["from"].stringValue + "-" + item["to"].stringValue
            self.courseTimeLB.text = selectedTime
            self.timeJson = item
            hideTimesB.isHidden = true
            UIView.animate(withDuration: 0.2) {
                self.timesView.isHidden = true
            }
            
        }
    }
    
    

    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chooseCourseTimeBTN(_ sender: Any) {
        hideTimesB.isHidden = false
        UIView.animate(withDuration: 0.2) {
            self.timesView.isHidden = false
        }
    }
    @IBAction func hideTimesViewBTN(_ sender: UIButton) {
        sender.isHidden = true
        UIView.animate(withDuration: 0.2) {
                  self.timesView.isHidden = true
              }
    }
    @IBAction func continueBTN(_ sender: Any) {
        guard timeJson != nil else {
            self.toast("There is no available time")
            return
        }
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "ConfirmReservationVC") as! ConfirmReservationVC
        vc.timeJson = timeJson!
        vc.params = params
        vc.techProfile = profile!
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
}
