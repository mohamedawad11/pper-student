
import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import DatePicker
class HomeVC: UIViewController ,GMSMapViewDelegate,CLLocationManagerDelegate{
    @IBOutlet weak var user_loc_img: UIImageView!
    @IBOutlet weak var teacher_loc_img: UIImageView!
    @IBOutlet weak var any_img: UIImageView!
    @IBOutlet weak var male_img: UIImageView!
    @IBOutlet weak var female_img: UIImageView!
    @IBOutlet weak var me_img: UIImageView!
    @IBOutlet weak var group_img: UIImageView!

    @IBOutlet weak var whenToStudyTF: UITextField!
    @IBOutlet weak var forWhoView: UIView!
    
    @IBOutlet weak var wishStackView: UIStackView!
    
    @IBOutlet weak var chooseGroup_b: UIButton!
    @IBOutlet weak var forMe_B: UIButton!
    @IBOutlet weak var forGroup_B: UIButton!
    @IBOutlet weak var teacherLoc_B: UIButton!
    @IBOutlet weak var userLoc_B: UIButton!
    @IBOutlet weak var genderFemale: UIButton!
    @IBOutlet weak var genderMale: UIButton!
    @IBOutlet weak var genderAny: UIButton!
    @IBOutlet weak var detailsView: BGView!
    @IBOutlet weak var viewMap: GMSMapView!
    private let locationManager = CLLocationManager()
    var lon:Double?
    var lat:Double?
    var subjects = [JSON]()
    var subjectsNames = [String]()
    var subjectID = "-1"
    var subjectName = ""
//    var levels = [JSON]()
//    var levelsNames = [String]()
    var levelID = "-1"
    let dev = UserDefaults.standard
    var groups = [JSON]()
    var groupsNames = [String]()
    var groupID = "0"
    var clidrens = ["1","2","3","4","5","6","7","8","9","10"]
    var max_num = "1"
    var groupSelected = -1
    var schedule_day = "-1"
    var gender = "any"
    var userTecherLocation = 1
    var collectionId = "0"
    override func viewWillAppear(_ animated: Bool) {
        if notificationType == 1{
            notificationType = -1
            guard let item = notificationJS else { return }
            let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                 vc.group_id = item["group_id"].stringValue
                 self.navigationController?.pushViewController(vc, animated: false)
        }else if notificationType == 2{
            notificationType = -1
            guard let item = notificationJS else { return }
            let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UpcomingReservationDetailsVC") as! UpcomingReservationDetailsVC
            vc.reservation_id = item["reservation_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: false)
        }else if ishaveReservation{
        self.haveMakeReservation()
        }
        if subjectID != "-1" && levelID != "-1"{
            returnGroups()
        }
    }
 
 
    override func viewDidLoad() {
        super.viewDidLoad()
    
        chooseGroup_b.isHidden = true
        if self.is_login(),let studentLevelID = dev.string(forKey: "student_level_id"){
            levelID = studentLevelID
        }
        //detailsView.isHidden = true
        if self.is_login(){
                returnSubjects(id: dev.string(forKey: "student_level_id")!)
        }
        // test
        NotificationCenter.default.addObserver(self, selector: #selector(groupNoti), name: NSNotification.Name(rawValue: "groupNoti"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(reservationNoti), name: NSNotification.Name(rawValue: "reservationNoti"), object: nil)
        
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            print("ennnnan")
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.startUpdatingLocation()
        }
        viewMap.delegate = self
        if self.is_login(){
            print("done")
        setSocketWithStudentroom()
        }
        whenToStudyTF.setInputViewDatePicker(target: self, mode: .date ,selector: #selector(whenCourse))
    }
    
    @objc func groupNoti(_ notification:Notification){
         guard let group_id = notification.userInfo!["id"] as? String else{return}
        notificationType = -1
         let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
              vc.group_id = group_id
              self.navigationController?.pushViewController(vc, animated: false)
     }
    @objc func reservationNoti(_ notification:Notification){
         guard let reservation_id = notification.userInfo!["id"] as? String else{return}
        let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UpcomingReservationDetailsVC") as! UpcomingReservationDetailsVC
        vc.reservation_id = reservation_id
        self.navigationController?.pushViewController(vc, animated: false)
     }
    
    func setSocketWithStudentroom(){
          SocketIOManager.sharedInstance.socket.emitWithAck("student_room", with: [UserDefaults.standard.string(forKey: "id") ?? "0"]).timingOut(after: 4) { (data) in
                 }
      }
    func returnSubjects(id:String){
        Helper_API.instance.subjectsUpOnLevel(id: id) { (code, result) in
               if code == 200,let data = result,data["success"].boolValue,data["data"].arrayValue.count > 0{
                   self.subjects = data["data"].arrayValue
                for item in data["data"].arrayValue{
                    self.subjectsNames.append(item["name"].stringValue)
                }
               }else if code == 401{
                   self.sessionExpired()
               }
           }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
       print("bnbbnnbbnnbnb")
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        viewMap.isMyLocationEnabled = true
        viewMap.settings.myLocationButton = true
        viewMap.settings.compassButton = true
    }
    var onLoc = true
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if onLoc{
            print("llllllloo")
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            self.locationManager.stopUpdatingLocation()
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            viewMap.camera = GMSCameraPosition(latitude: latitude, longitude: longitude, zoom: 15.0, bearing: 6, viewingAngle: 100)
            onLoc = false
        }
        }
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let coordinate = position.target
        lat = coordinate.latitude
        lon = coordinate.longitude
        viewMap.clear()
        //reverseGeocodeCoordinate(coordinate)
        let markerSquirt = GMSMarker()
        markerSquirt.position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
        markerSquirt.icon = UIImage(named: "marker")
        markerSquirt.map = viewMap
    }
    @IBAction func preferredLocation_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            user_loc_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            user_loc_img.image = UIImage(named: "c2")
            teacher_loc_img.tintColor = UIColor(named: "LableColor")!
            teacher_loc_img.image = UIImage(named: "c1")
            teacherLoc_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)

        case 1:
            teacher_loc_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            teacher_loc_img.image = UIImage(named: "c2")
            user_loc_img.tintColor = UIColor(named: "LableColor")!
            userLoc_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            user_loc_img.image = UIImage(named: "c1")
        default:
            print("no thing")
        }
    }
    @IBAction func teacherGender_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            any_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)

            male_img.tintColor = UIColor(named: "LableColor")!
            genderMale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            female_img.tintColor = UIColor(named: "LableColor")!
            genderFemale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        case 1:
            male_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            
            any_img.tintColor = UIColor(named: "LableColor")!
            genderAny.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            female_img.tintColor = UIColor(named: "LableColor")!
            genderFemale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        case 2:
            female_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            
            any_img.tintColor = UIColor(named: "LableColor")!
            genderAny.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            male_img.tintColor = UIColor(named: "LableColor")!
            genderMale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        default:
            print("no thing")
        }
    }
    @IBAction func forWho_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            groupSelected = 0
            chooseGroup_b.isHidden = true
            me_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
             group_img.tintColor = UIColor(named: "LableColor")!
            forGroup_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        case 1:
            groupSelected = -1
            chooseGroup_b.isHidden = false
            group_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
             me_img.tintColor = UIColor(named: "LableColor")!
            forMe_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        default:
            print("no thing")
        }
    }
//    @IBAction func whenStudy_btn(_ sender: UIButton) {
//        let datePicker = DatePicker()
//              datePicker.setup(beginWith: Date()) { (selected, date) in
//                  if selected, let ssDate = date {
//                    let dateformate = DateFormatter()
//                    if self.is_ar() == "ar"{
//                        dateformate.locale = Locale(identifier: "ar_EG")
//                    }
//                    dateformate.dateFormat = "yyyy-MM-dd"
//                    sender.setTitle(dateformate.string(from: ssDate), for: .normal)
//                    if self.is_ar() == "ar"{
//                        dateformate.locale = Locale(identifier: "en-us")
//                    }
//                    self.schedule_day = dateformate.string(from: ssDate)
//                  } else {
//                      print("Cancelled")
//                  }
//              }
//              datePicker.show(in: self, on: sender)
//    }
    
    @objc func whenCourse() {
        if let datePicker = whenToStudyTF.inputView as? UIDatePicker {
            let dateformate = DateFormatter()
            if self.is_ar() == "ar"{
                dateformate.locale = Locale(identifier: "ar_EG")
            }
            dateformate.dateFormat = "yyyy-MM-dd"
            whenToStudyTF.text = dateformate.string(from: datePicker.date)
            datePicker.preferredDatePickerStyle = .wheels
            if self.is_ar() == "ar"{
                dateformate.locale = Locale(identifier: "en-us")
            }
            self.schedule_day = dateformate.string(from: datePicker.date)
            }
            whenToStudyTF.resignFirstResponder()
    }
    
    @IBAction func chooseWhatStudy_btn(_ sender: UIButton) {
        if dev.bool(forKey: "is_login"){
        self.drop(anchor: wishStackView, dataSource: subjectsNames) { (item, index) in
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor(named: "primaryColor"), for: .normal)
            self.subjectName = item
            self.subjectID = self.subjects[index]["id"].stringValue
            self.returnGroups()
        }
        }
    }
    
    @IBAction func showHide_btn(_ sender: UIButton) {
        if detailsView.isHidden{
            detailsView.isHidden = false
        }else{
            detailsView.isHidden = true
        }
    }
    
    @IBAction func sideMenu_btn(_ sender: Any) {
        let vc = helperHelper.sideMenu("Home", "sideMenuID")
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func chooseGroup_btn(_ sender: UIButton) {
        guard groups.count > 0 else {
            self.addgroupFunc()
            return
        }
        if subjectID != "-1"{
        self.drop(anchor: forWhoView, dataSource: self.groupsNames) { (item, index) in
            sender.setTitle(item, for: .normal)
            self.groupID = self.groups[index]["id"].stringValue
            self.max_num = "\(self.groups[index]["collection_num"].intValue)"
        }
        }else{
            self.toast("Choose Subject")
        }
    }
    
    func returnGroups(){
        groupsNames.removeAll()
        let params:[String:Any] = [
            "student_id":dev.string(forKey: "id") ?? "",
            "level_id":levelID,
            "subject_id":subjectID
        ]
        Helper_API.instance.postActionWithAuth(url: studentForSearchGroupsURL, params: params) { (code, result) in
            
            print("nmvmnbnmvnmbnvnm \(result?["success"].boolValue)")
            if code == 200,let data = result,data["success"].boolValue{
                self.groups = data["data"].arrayValue
                for item in data["data"].arrayValue{
                    self.groupsNames.append(item["collection_name"].stringValue)
                }
            }
        }
    }
    
    @IBAction func go_btn(_ sender: UIButton) {
        guard self.is_login() else {
            self.login()
            return
        }
        guard subjectID != "-1" else {
            self.toast("Choose Subject")
            return
        }
        guard schedule_day != "-1" else {
            self.toast("Choose Date")
            return
        }
        guard groupSelected != 1 || groupID != "0" else {
            self.toast("Choose Group")
            return
        }
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SearchForTeacherVC") as! SearchForTeacherVC
        let params:[String:Any] = [
                "subject_id":subjectID,
                "level_id":levelID,
                "schedule_day":schedule_day,
                "collectionId":groupID,
                "gender":gender,
                "userTecherLocation":userTecherLocation,
                "max_num":max_num,
                "subjectName":subjectName
            ]
        vc.params = params
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
