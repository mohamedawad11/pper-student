

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import DatePicker

class ParentHomeVC: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate{
    @IBOutlet weak var user_loc_img: UIImageView!
    @IBOutlet weak var teacher_loc_img: UIImageView!
    @IBOutlet weak var any_img: UIImageView!
    @IBOutlet weak var male_img: UIImageView!
    @IBOutlet weak var female_img: UIImageView!
    @IBOutlet weak var me_img: UIImageView!
    @IBOutlet weak var group_img: UIImageView!
    @IBOutlet weak var whenToStudyTF: UITextField!

    @IBOutlet weak var forWhoView: UIView!
    @IBOutlet weak var wishStackView: UIStackView!
    @IBOutlet weak var levelStackView: UIStackView!
    @IBOutlet weak var chooseGroup_b: UIButton!
    @IBOutlet weak var forMe_B: UIButton!
    @IBOutlet weak var forGroup_B: UIButton!
    @IBOutlet weak var teacherLoc_B: UIButton!
    @IBOutlet weak var userLoc_B: UIButton!
    @IBOutlet weak var genderFemale: UIButton!
    @IBOutlet weak var genderMale: UIButton!
    @IBOutlet weak var genderAny: UIButton!
    @IBOutlet weak var detailsView: BGView!
    @IBOutlet weak var viewMap: GMSMapView!
    private let locationManager = CLLocationManager()
    var lon:Double?
    var lat:Double?
    var subjects = [JSON]()
    var subjectsNames = [String]()
    var subjectID = "-1"
    var subjectName = ""
    var levels = [JSON]()
    var levelsNames = [String]()
    var levelID = "-1"
    let dev = UserDefaults.standard
    
    var groups = [JSON]()
    var groupsNames = [String]()
    var groupID = "0"
    var clidrens = ["1","2","3","4","5","6","7","8","9","10"]
    var max_num = "1"
    var groupSelected = 0
    var schedule_day = "-1"
    var gender = "any"
    var userTecherLocation = 1
    var collectionId = "0"
    override func viewWillAppear(_ animated: Bool) {
        if notificationType == 1{
            notificationType = -1
            guard let item = notificationJS else { return }
            let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                 vc.group_id = item["group_id"].stringValue
                 self.navigationController?.pushViewController(vc, animated: false)
        }else if notificationType == 2{
            notificationType = -1
            guard let item = notificationJS else { return }
            let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UpcomingReservationDetailsVC") as! UpcomingReservationDetailsVC
            vc.reservation_id = item["reservation_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: false)
        }else if ishaveReservation{
        self.haveMakeReservation()
        }

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            print("ennnnan")
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.startUpdatingLocation()
        }
        viewMap.delegate = self
       // viewMap.isUserInteractionEnabled = false
        if self.is_login(){
            print("done")
        setSocketWithStudentroom()
        }
        whenToStudyTF.setInputViewDatePicker(target: self, mode: .date ,selector: #selector(whenCourse))
        returnLevels()
    }
    @objc func whenCourse() {
        if let datePicker = whenToStudyTF.inputView as? UIDatePicker {
            let dateformate = DateFormatter()
            if self.is_ar() == "ar"{
                dateformate.locale = Locale(identifier: "ar_EG")
            }
            dateformate.dateFormat = "yyyy-MM-dd"
            whenToStudyTF.text = dateformate.string(from: datePicker.date)
            datePicker.preferredDatePickerStyle = .wheels
            if self.is_ar() == "ar"{
                dateformate.locale = Locale(identifier: "en-us")
            }
            self.schedule_day = dateformate.string(from: datePicker.date)
            }
            whenToStudyTF.resignFirstResponder()
    }
    
    func setSocketWithStudentroom(){
          SocketIOManager.sharedInstance.socket.emitWithAck("student_room", with: [UserDefaults.standard.string(forKey: "id") ?? "0"]).timingOut(after: 4) { (data) in
                 }
      }
    func returnLevels(){
        print("mcvmmcvcnmvverrrrr")
        Helper_API.instance.levels { (code, result) in
             if code == 200,let data = result,data["success"].boolValue,data["data"].arrayValue.count > 0{
                self.levels = data["data"].arrayValue
                for item in data["data"].arrayValue{
                    self.levelsNames.append(item["name"].stringValue)
                }
                
             }else{
                self.toast("internet")
            }
        }
    }
    func returnSubjects(id:String){
        Helper_API.instance.subjectsUpOnLevel(id: id) { (code, result) in
               if code == 200,let data = result,data["success"].boolValue,data["data"].arrayValue.count > 0{
                   self.subjects = data["data"].arrayValue
                for item in data["data"].arrayValue{
                    self.subjectsNames.append(item["name"].stringValue)
                }
               }else{
                   self.toast("internet")
               }
           }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      print("dffjhdjhfjhfhjfhjfjhdfjh")
        guard status == .authorizedWhenInUse else {
            return
        }
        print("mvncvcnmcvmncvmnv")
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        viewMap.isMyLocationEnabled = true
        viewMap.settings.myLocationButton = true
        viewMap.settings.compassButton = true

    }
    var onLoc = true
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("no thing")
        if onLoc{
            print("mbvmbmmmmm")
        let location = locations[locations.count - 1]
        if location.horizontalAccuracy > 0 {
            self.locationManager.stopUpdatingLocation()
            let latitude = location.coordinate.latitude
            let longitude = location.coordinate.longitude
            viewMap.camera = GMSCameraPosition(latitude: latitude, longitude: longitude, zoom: 15.0, bearing: 6, viewingAngle: 100)
            onLoc = false
        }
        }
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let coordinate = position.target
        lat = coordinate.latitude
        lon = coordinate.longitude
        viewMap.clear()
        //reverseGeocodeCoordinate(coordinate)
        let markerSquirt = GMSMarker()
        markerSquirt.position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
        markerSquirt.icon = UIImage(named: "marker")
        markerSquirt.map = viewMap
    }
    @IBAction func preferredLocation_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            user_loc_img.tintColor = UIColor(named: "OrColor")!
            user_loc_img.image = UIImage(named: "c2")
            teacherLoc_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            teacher_loc_img.tintColor = UIColor(named: "LableColor")!
            teacher_loc_img.image = UIImage(named: "c1")

        case 1:
            teacher_loc_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            teacher_loc_img.image = UIImage(named: "c2")
            userLoc_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            user_loc_img.tintColor = UIColor(named: "LableColor")!
            user_loc_img.image = UIImage(named: "c1")
        default:
            print("no thing")
        }
    }
    @IBAction func teacherGender_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            any_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            male_img.tintColor = UIColor(named: "LableColor")!
            genderMale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            female_img.tintColor = UIColor(named: "LableColor")!
            genderFemale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        case 1:
            male_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            any_img.tintColor = UIColor(named: "LableColor")!
            genderAny.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            female_img.tintColor = UIColor(named: "LableColor")!
            genderFemale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        case 2:
            female_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
            any_img.tintColor = UIColor(named: "LableColor")!
            genderAny.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
            male_img.tintColor = UIColor(named: "LableColor")!
            genderMale.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        default:
            print("no thing")
        }
    }
    @IBAction func forWho_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            groupSelected = 0
            chooseGroup_b.setTitle("No Of Children".localized(), for: .normal)
            me_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
             group_img.tintColor = UIColor(named: "LableColor")!
            forGroup_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        case 1:
            groupSelected = 1
            chooseGroup_b.setTitle("Choose Group".localized(), for: .normal)
            group_img.tintColor = UIColor(named: "OrColor")!
            sender.setTitleColor(UIColor(named: "OrColor")!, for: .normal)
             me_img.tintColor = UIColor(named: "LableColor")!
            forMe_B.setTitleColor(UIColor(named: "LableColor")!, for: .normal)
        default:
            print("no thing")
        }
    }
    @IBAction func whenStudy_btn(_ sender: UIButton) {
             let datePicker = DatePicker()
               datePicker.setup(beginWith: Date()) { (selected, date) in
                   if selected, let ssDate = date {
                     let dateformate = DateFormatter()
                     if self.is_ar() == "ar"{
                         dateformate.locale = Locale(identifier: "ar_EG")
                     }
                     dateformate.dateFormat = "yyyy-MM-dd"
                     sender.setTitle(dateformate.string(from: ssDate), for: .normal)
                     if self.is_ar() == "ar"{
                         dateformate.locale = Locale(identifier: "en-us")
                     }
                     self.schedule_day = dateformate.string(from: ssDate)
                   } else {
                       print("Cancelled")
                   }
               }
               datePicker.show(in: self, on: sender)
    }
    

    @IBAction func chooseWhatStudy_btn(_ sender: UIButton) {
        if levelID != "-1"{
        self.drop(anchor: wishStackView, dataSource: subjectsNames) { (item, index) in
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor(named: "primaryColor"), for: .normal)
            self.subjectName = item
            self.subjectID = self.subjects[index]["id"].stringValue
            self.returnGroups()
        }
        }else{
            self.toast("Choose Level")
        }
    }
    
    @IBAction func showHide_btn(_ sender: UIButton) {
        if detailsView.isHidden{
            detailsView.isHidden = false
        }else{
            detailsView.isHidden = true
        }
    }
    @IBAction func chooseLevel_btn(_ sender: UIButton) {
        self.drop(anchor:levelStackView , dataSource: self.levelsNames) { (item, index) in
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor(named: "primaryColor"), for: .normal)
            self.levelID = self.levels[index]["id"].stringValue
            self.returnSubjects(id: self.levelID)
        }
    }

    @IBAction func sideMenu_btn(_ sender: Any) {
        let vc = helperHelper.sideMenu("Home", "sideMenuID")
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func chooseGroup_btn(_ sender: UIButton) {
        if subjectID != "-1"{
        if groupSelected == 1{
            if dev.bool(forKey: "is_login"){
        self.drop(anchor: forWhoView, dataSource: self.groupsNames) { (item, index) in
            sender.setTitle(item, for: .normal)
            self.groupID = self.groups[index]["id"].stringValue
            self.max_num = "\(self.groups[index]["collection_num"].intValue)"
        }
            }else{
                self.toast("You have to sign in to unlock this feature")
            }
        }else{
            self.drop(anchor: forWhoView, dataSource: self.clidrens) { (item, index) in
                      sender.setTitle(item + " " + "Child".localized(), for: .normal)
                           self.max_num = item
                self.groupID = "0"
                       }
        }
        }else{
            self.toast("Choose Subject")
        }
    }
    func returnGroups(){
        let params:[String:Any] = [
            "student_id":dev.string(forKey: "id") ?? "",
            "level_id":levelID,
            "subject_id":subjectID
        ]
        Helper_API.instance.postActionWithAuth(url: studentForSearchGroupsURL, params: params) { (code, result) in
            if code == 200,let data = result,data["success"].boolValue{
                self.groups = data["data"].arrayValue
                for item in data["data"].arrayValue{
                    self.groupsNames.append(item["collection_name"].stringValue)
                }
            }
        }
    }
    
    @IBAction func go_btn(_ sender: UIButton) {
        guard self.is_login() else {
            self.login()
            return
        }
        guard levelID != "0" else {
            self.toast("Choose Level")
            return
        }
        guard subjectID != "-1" else {
            self.toast("Choose Subject")
            return
        }
        guard schedule_day != "-1" else {
            self.toast("Choose Date")
            return
        }
        guard groupSelected != 1 || groupID != "0" else {
                   self.toast("Choose Group")
                   return
               }
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SearchForTeacherVC") as! SearchForTeacherVC
        let params:[String:Any] = [
                "subject_id":subjectID,
                "level_id":levelID,
                "schedule_day":schedule_day,
                "collectionId":groupID,
                "gender":gender,
                "userTecherLocation":userTecherLocation,
                "max_num":max_num,
                "subjectName":subjectName
            ]
        vc.params = params
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
