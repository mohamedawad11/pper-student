
import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
class SearchForTeacherVC: UIViewController ,GMSMapViewDelegate,CLLocationManagerDelegate{
    
    //
    //    @IBOutlet weak var tableView: UITableView!
    //    @IBOutlet weak var noDataLB: UILabel!
    //    @IBOutlet weak var noDataView: BGView!
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var searcB: UIButton!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var searchTF: UILabel!
    var params = [String:Any]()
    private let locationManager = CLLocationManager()
//    var lon:Double?
//    var lat:Double?
    
    var lat:Double? = 30.504021
    var lon:Double? = 31.100639
    var userLat = 0.0
    var userLon = 0.0
    var address = ""
    var teachersModel = [Teachers]()
    var location:CLLocation?
    var geocoder = CLGeocoder()
    var placeMark:CLPlacemark?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled(){
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
            locationManager.startUpdatingLocation()
        }
        viewMap.delegate = self
    }
    
    
    @IBAction func searchTapped(_ sender:UIButton){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else {
            return
        }
        print("mnmbmvmnbvmnnmb")
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        viewMap.isMyLocationEnabled = true
        viewMap.settings.myLocationButton = true
        viewMap.settings.compassButton = true
    }
    
    var oneLoc = true
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if oneLoc {
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            print("locations \(locValue.latitude) \(locValue.longitude)")
            
            let userLocation = locations.last!
            lat = userLocation.coordinate.latitude
            lon = userLocation.coordinate.longitude
            self.userLat = lat ?? 0.0
            self.userLon = lon ?? 0.0
            let camera = GMSCameraPosition.camera(withLatitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude, zoom: 17.0)
            viewMap.camera = camera
            reverseGeocodeCoordinate(locValue)
            // Add a Custom marker
            let markerSquirt = GMSMarker()
            markerSquirt.position = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
            markerSquirt.map = viewMap
            oneLoc = false
            findTeacher()
        }      }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        let coordinate = position.target
        print("You tapped at \(coordinate.latitude), \(coordinate.longitude)")
        lat = coordinate.latitude
        lon = coordinate.longitude
//        viewMap.clear()
        reverseGeocodeCoordinate(coordinate)
        //self.searcB.isHidden = false
        
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
//        marker.icon = UIImage(named: "marker")
//        marker.map = viewMap
        
    }
    
    func getaddress(_ place:GMSAddress){
        self.searchTF.text = place.lines?.joined(separator: "\n")
    }
    private func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let addres = response?.firstResult() {
                self.getaddress(addres)
            }
            
            if let error = error{
                print("ererf ",error.localizedDescription)
            }
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    func findTeacher(){
        guard userLat != 0.0 , userLon != 0.0 else {
            return
        }
        let par:[String:Any] = [
            "subject_id":params["subject_id"]! as! String,
            "level_id":params["level_id"]! as! String,
            "schedule_day":params["schedule_day"]! as! String,
            "max_num":params["max_num"]! as! String,
            "gender":params["gender"]! as! String,
            "lat":self.userLat,
            "lon":self.userLon
        ]
        
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: findTeacherURL, params: par) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200,let data = result{
                guard data["success"].boolValue else{
                    self.to_ast(data["message"].stringValue)
                    return}
                do{
                    let successData = try data["data"].rawData()
                    let decoder = JSONDecoder()
                    self.teachersModel = try decoder.decode([Teachers].self, from: successData)
                    self.setTeachersOnMap(self.teachersModel)
                }catch{
                    print(error.localizedDescription , "Error in decoding")
                }
            }else{
                self.toast("internet")
            }
        }
    }
    func setTeachersOnMap(_ teachers:[Teachers]){
        viewMap.clear()
        for item in teachers{
            let map = Bundle.main.loadNibNamed("TeacherOnMapView", owner: self, options: nil)?.first as! TeacherOnMapView
            map.distanceLB.text = "\(item.the0?.distance ?? "0") \("KM".localized())"
            if let photo = item.member?.photo,let teachID = item.member?.id{
                map.logo.sd_setImage(with: URL(string: "\(imageURL)\(teachID)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
            }else{
                map.logo.image = UIImage(named: "profilePlaceHolder")
            }
            map.nameLB.text = "\(item.member?.fName ?? "") \(item.member?.mName ?? "") \(item.member?.lName ?? "")"
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: ((item.member?.lat!)! as NSString).doubleValue, longitude: ((item.member?.lon!)! as NSString).doubleValue)
            marker.iconView = map
            marker.map = viewMap
            marker.userData = item
        }
        drawCircleOnMap(viewMap, userLat, userLon)
    }
    func drawCircleOnMap(_ map:GMSMapView,_ la:Double,_ lo:Double){
        map.camera = GMSCameraPosition(latitude: la, longitude: lo, zoom: 12.4, bearing: 15, viewingAngle: 70)
        let circleCenter = CLLocationCoordinate2DMake(la, lo)
        let circle = GMSCircle()
        circle.position = circleCenter
         circle.fillColor = UIColor(red: 1.000, green: 0.369, blue: 0.227, alpha: 0.1)
         circle.strokeWidth = 3
         circle.strokeColor = UIColor(named: Colors.instance.mainColor)
         circle.map = map
        circle.radius = 5000
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let data = marker.userData as? Teachers{
            print("vnmccvcvn \(data)")
            
            let para:[String:Any] = [
                "teacher_id":data.member?.id ?? "",
                "level_id":params["level_id"]! as? String ?? "",
                "subject_id":params["subject_id"]! as? String ?? ""
            ]
            BGLoading.instance.showLoading(view)
            Helper_API.instance.postActionWithAuth(url: teacherProfileURL, params: para) { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200,let data = result{
                    guard data["success"].boolValue else {
                        self.to_ast(data["message"].stringValue)
                        return
                    }
                    do{
                        let jsonData = try data["data"].rawData()
                        let decoder = JSONDecoder()
                        let teacherProdile = try decoder.decode(TeacherProfileModel.self, from: jsonData)
                        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "TeacherProfileVC") as! TeacherProfileVC
                        vc.profile = teacherProdile
                        vc.params = self.params
                        self.navigationController?.pushViewController(vc, animated: false)
                    }catch{
                        print(error.localizedDescription,"Error in decoding")
                    }
                }
            }
        }
        return true
    }
    
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func confirm_btn(_ sender: Any) {
        guard lat != nil , lon != nil else {
            return
        }
        self.userLat = lat ?? 0.0
        self.userLon = lon ?? 0.0
        self.findTeacher()
    }
    
    
}

extension SearchForTeacherVC: GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        lat = place.coordinate.latitude
        lon = place.coordinate.longitude
        address = place.name ?? ""
        self.searchTF.text = address
        //    self.reverseGeocodeCoordinate(place.coordinate)
        let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lon!, zoom: 17.0)
        self.viewMap.camera = camera
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
        marker.icon = UIImage(named: "marker")
        marker.map = self.viewMap
        print("seeelllll")
        self.userLat = lat!
        self.userLon = lon!
        self.findTeacher()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    
}
