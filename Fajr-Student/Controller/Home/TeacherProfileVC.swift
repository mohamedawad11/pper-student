
import UIKit

class TeacherProfileVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
  
    
    
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var subjectLB: UILabel!
    @IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var clockLB: UILabel!
    @IBOutlet weak var teacherNameLB: UILabel!
    @IBOutlet weak var ProfileImg: UIImageView!
    @IBOutlet weak var logoImg: UIImageView!
    
    @IBOutlet weak var emptyListStack: UIStackView!
    
    @IBOutlet weak var tableConst: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var profile:TeacherProfileModel?
    var params = [String:Any]()
    var reviews = [ReviewsModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        emptyListStack.isHidden = true
        tableView.register(UINib(nibName: "ReviewCell", bundle: nil), forCellReuseIdentifier: "ReviewCell")
        tableView.delegate = self
        tableView.dataSource = self
        if self.is_ar() == "ar"{
                   backB.setImage(UIImage(named: "ll"), for: .normal)
               }
        if let item = profile{
            if let photo = item.photo,let teachID = item.id{
                logoImg.sd_setImage(with: URL(string: "\(imageURL)\(teachID)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
                ProfileImg.sd_setImage(with: URL(string: "\(imageURL)\(teachID)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
                ProfileImg.blurImage()
            }else{
               logoImg.image = UIImage(named: "profilePlaceHolder")
            }
            teacherNameLB.text = "\(item.fName ?? "") \(item.mName ?? "") \(item.lName ?? "")"
            priceLB.text = "\(item.costPerStudent ?? "") \(currencyTXT)"
            clockLB.text = "\(item.deurationPerHoures ?? "") \("Hours".localized())"
            subjectLB.text = params["subjectName"] as? String ?? ""
            getReviews(item.subjectID!)
        }

        
    }
    
    @IBAction func reserveBTN(_ sender: Any) {
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CourseTimeVC") as! CourseTimeVC
        vc.profile = profile!
        vc.params = params
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getReviews(_ subject_id:String){
        Helper_API.instance.getFunc(url: subjectReviewsURL + "\(subject_id)/0") { (code, result) in
            if code == 200 ,let data = result{
                if data["success"].boolValue,data["data"].arrayValue.count > 0 {
                    self.tableView.isHidden = false
                    self.emptyListStack.isHidden = true
                    do{
                        let jsonData = try data["data"].rawData()
                        let decoder = JSONDecoder()
                        self.reviews = try decoder.decode([ReviewsModel].self, from: jsonData)
                        self.tableView.reloadData()
                        self.tableConst.constant = CGFloat(self.reviews.count * 90)
                    }catch{
                        print(error.localizedDescription,"error in decoding data")
                    }
                }else{
                    self.tableView.isHidden = true
                    self.emptyListStack.isHidden = false
                }
            }else{
                self.tableView.isHidden = true
                self.emptyListStack.isHidden = false
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as? ReviewCell ,reviews.count > 0 else {
            return UITableViewCell()
        }
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let pos = indexPath.row
        let item = reviews[pos]
        cell.dateLB.text = item.review?.created ?? ""
        cell.commentLB.text = item.review?.reviewBody ?? ""
        cell.rateView.rating = (item.review!.rate! as NSString).doubleValue
        cell.nameLB.text = item.student?.fName ?? ""
        if let photo = item.student?.photo,let teachID = item.student?.id{
            cell.logo.sd_setImage(with: URL(string: "\(imageURL)\(teachID)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
        }else{
            cell.logo.image = UIImage(named: "profilePlaceHolder")
        }
        return cell
      }
      
    
}


extension UIImageView{
    
    func blurImage() {
        let darkBlur = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.bounds
        self.addSubview(blurView)
    }
}
