

import UIKit

class ContactUsVC: UIViewController {
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var txtLB: UILabel!
    
    @IBOutlet weak var contentLB: UITextField!
    @IBOutlet weak var titleLB: UITextField!
    let dev = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        txtLB.setLineHeight(lineHeight: 1.5)
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
    }
    @IBAction func sendBTN(_ sender: Any) {
        guard !titleLB.text!.isEmpty else {
            self.toast("Enter Message Title")
            return
        }
        guard !contentLB.text!.isEmpty else {
                   self.toast("Enter Message Content")
                   return
               }
        let params = [
            "title":titleLB.text!,
            "body":contentLB.text!,
            "email":dev.string(forKey: "email") ?? ""
        ]
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: contactUsURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200,let data = result{
                if data["success"].boolValue{
                    self.toast("Send Successfully")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.navigationController?.popViewController(animated: true)
                    }
                }else{
                    self.to_ast(data["message"].stringValue)
                }
            }else{
                self.toast("internet")
            }
        }
    }
    

    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
