

import UIKit
import SwiftyJSON
class EditAccountVC: UIViewController {
    @IBOutlet weak var educationStackView: UIStackView!
    @IBOutlet weak var innerOther: UIView!
    @IBOutlet weak var otherView: BGView!
    @IBOutlet weak var educationView: BGView!
    @IBOutlet weak var innerEducation: UIView!
    @IBOutlet weak var basicInfoView: BGView!
    @IBOutlet weak var innerBasicInfo: UIView!
    @IBOutlet weak var back_b: UIButton!
    @IBOutlet weak var firstName_LB: UITextField!
    @IBOutlet weak var lastName_LB: UITextField!
    @IBOutlet weak var phone_LB: UITextField!
    @IBOutlet weak var email_LB: UITextField!

    @IBOutlet weak var level_b: UIButton!
    @IBOutlet weak var basicInfo_b: UIButton!
    @IBOutlet weak var education_b: UIButton!
    @IBOutlet weak var others_b: UIButton!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var language_LB: UILabel!
    let dev = UserDefaults.standard
    var levels = [JSON]()
      var levelsNames = [String]()
      var levelID = "-1"
    override func viewDidLoad() {
        super.viewDidLoad()
        if dev.bool(forKey: "is_parent"){
            educationView.isHidden = true
        }
        if self.is_ar() == "ar"{
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
        if is_login() {
            if let photo = dev.string(forKey: "photo"){
                logo.sd_setImage(with: URL(string: "\(imageURL)\(dev.string(forKey: "id")!)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
            }
            if let f_name = dev.string(forKey: "f_name"),let l_name = dev.string(forKey: "l_name"){
                firstName_LB.text = f_name
                lastName_LB.text = l_name
            }
            if let phone = dev.string(forKey: "phone"){
                           phone_LB.text = phone
                       }
            if let email = dev.string(forKey: "email"){
                email_LB.text = email
            }
            if self.is_ar() == "ar"{
                language_LB.text = "العربيه"
                level_b.setTitle(dev.string(forKey: "student_level_name_ara") ?? "", for: .normal)
            }else{
                level_b.setTitle(dev.string(forKey: "student_level_name_eng") ?? "", for: .normal)
                language_LB.text = "English"

            }
            levelID = dev.string(forKey: "student_level_id") ?? ""
            
        }
         setData()
        returnLevels()
    }
    func setData(){
        basicInfoView.layer.cornerRadius = 10.0
        basicInfoView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        educationView.layer.cornerRadius = 10.0
        educationView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        otherView.layer.cornerRadius = 10.0
        otherView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        innerBasicInfo.isHidden = true
        innerEducation.isHidden = true
        innerOther.isHidden = true
    }
    @IBAction func showHideView_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            if innerBasicInfo.isHidden{
            innerBasicInfo.isHidden = false
            }else{
            innerBasicInfo.isHidden = true
            }
        case 1:
            if innerEducation.isHidden{
                innerEducation.isHidden = false
            }else{
                innerEducation.isHidden = true
            }
        case 2:
            if innerOther.isHidden{
                innerOther.isHidden = false
            }else{
                innerOther.isHidden = true
            }
        default:
            print("no thing")
        }
    }
    @IBAction func addProfileLogo(_ sender: UIButton) {
        ImagePickerManager().pickImage(self){ image in
            self.logo.image = image
            self.logo.contentMode = .scaleToFill
            let images = ["photo":image]
            let params = ["id":UserDefaults.standard.string(forKey: "id")!]
            BGLoading.instance.showLoading(self.view)
            Auth_API.instance.uploadPersonalImage(param: params, images: images) { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200 ,let data = result{
                    if data["success"].boolValue{
                        print("uploaded successfully")
                        UserDefaults.standard.set(data["data"]["imageName"].stringValue, forKey: "photo")
                    }else{
                        self.to_ast(data["data"]["message"].stringValue)
                    }
                }else{
                    self.toast("internet")
                }
            }
        }
    }
    @IBAction func level_btn(_ sender: UIButton) {
        self.drop(anchor: sender, dataSource: levelsNames) { (item, index) in
            sender.setTitle(item, for: .normal)
            self.levelID = self.levels[index]["id"].stringValue
        }
    }
    func returnLevels(){
        Helper_API.instance.levels { (code, result) in
             if code == 200,let data = result,data["success"].boolValue,data["data"].arrayValue.count > 0{
                self.levels = data["data"].arrayValue
                for item in data["data"].arrayValue{
                    self.levelsNames.append(item["name"].stringValue)
                }
                
             }else{
                self.toast("internet")
            }
        }
    }
    
    func validateBasicInfo()->Bool{
        guard !firstName_LB.text!.isEmpty else {
            self.toast("Enter First Name")
            return false
        }
        guard !lastName_LB.text!.isEmpty else {
                   self.toast("Enter Last Name")
                   return false
               }
        guard !email_LB.text!.isEmpty else {
                          self.toast("Enter Email Address")
                          return false
                      }
        guard !phone_LB.text!.isEmpty else {
                                 self.toast("Enter Phone Number")
                                 return false
                             }
        return true
    }
    
    @IBAction func save_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            print("save basic info")
            guard validateBasicInfo() else {
                return
            }
            let par:[String:Any] = [
                "id":dev.string(forKey: "id") ?? "",
                "f_name":firstName_LB.text ?? "",
                "l_name":lastName_LB.text ?? "",
                "email":email_LB.text ?? "",
                "phone":phone_LB.text ?? "",
                "student_level_id":dev.string(forKey: "student_level_id") ?? "",
                "is_parent":dev.bool(forKey: "is_parent") ? 1 : 0
            ]
            BGLoading.instance.showLoading(view)
            Helper_API.instance.postActionWithAuth(url: updateBasicInfoURL, params: par) { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200,let data = result{
                    if data["success"].boolValue{
                        
                        self.dev.set(self.firstName_LB.text!, forKey: "f_name")
                        self.dev.set(self.lastName_LB.text!, forKey: "l_name")
                        self.dev.set(self.email_LB.text!, forKey: "email")
                        self.dev.set(self.phone_LB.text!, forKey: "phone")
                    }
                    self.to_ast(data["data"]["message"].stringValue)
                    
                }else{
                    self.toast("internet")
                }
            }
            
        case 1:
            print("save level")
            let par:[String:Any] = [
                           "id":dev.string(forKey: "id") ?? "",
                           "f_name":dev.string(forKey: "f_name") ?? "",
                           "l_name":dev.string(forKey: "l_name") ?? "",
                           "email":dev.string(forKey: "email") ?? "",
                           "phone":dev.string(forKey: "phone") ?? "",
                           "student_level_id":levelID,
                           "is_parent":dev.bool(forKey: "is_parent") ? 1 : 0
                       ]
                       BGLoading.instance.showLoading(view)
                       Helper_API.instance.postActionWithAuth(url: updateBasicInfoURL, params: par) { (code, result) in
                           BGLoading.instance.dismissLoading()
                           if code == 200,let data = result{
                               self.to_ast(data["data"]["message"].stringValue)
                           }else{
                               self.toast("internet")
                           }
                       }
        default:
            print("no thing")
        }
    }
    @IBAction func changeLAnguage_btn(_ sender: Any) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                 return
             }
             if UIApplication.shared.canOpenURL(settingsUrl) {
                 UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                     print("Settings opened: \(success)") // Prints true
                 })
             }
        
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
