

import UIKit
import SwiftyJSON
class HelpVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var messageTF: UITextField!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var tableconst: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var faqs = [JSON]()
    var isShow = [Bool]()
    let dev = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        tableconst.constant = 0.0
        tableView.register(UINib(nibName: "FaqCell", bundle: nil), forCellReuseIdentifier: "FaqCell")
        tableView.delegate = self
        tableView.dataSource = self
        let params:[String:Any] = [
            "teacher_student":0,
            "lang":self.is_ar() == "ar" ? "ara":"eng"
        ]
        Helper_API.instance.postActionWithAuth(url: faqsURL, params: params) { (code, result) in
            if code == 200 ,let data = result,data["success"].boolValue{
                for item in data["data"].arrayValue{
                    self.isShow.append(false)
                    self.faqs.append(item["FaqAdmin"])
                }
                self.tableView.reloadData()
                self.tableconst.constant = CGFloat(self.faqs.count * 70)
            }else{
                print("fail")
            }
        }
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell", for: indexPath) as? FaqCell,faqs.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = faqs[pos]
            let show = isShow[pos]
            cell.titleLB.text = item["question"].stringValue
            cell.contentLB.text = item["answer"].stringValue.htmlToString
            cell.contentLB.setLineHeight(lineHeight: 1.5)
            cell.showBTN.tag = pos
            cell.showBTN.addTarget(self, action: #selector(showTapped), for: .touchUpInside)
            if show{
                cell.faqView.isHidden = false
            }else{
                cell.faqView.isHidden = true
            }
            print("cmbmvmnb \(cell.contentLB.isHeight())")
            return cell
        }else{
            return UITableViewCell()
        }
    }
    @objc func showTapped(_ sender:UIButton){
        let pos = sender.tag
        let cell = tableView.cellForRow(at: IndexPath(row: pos, section: 0)) as! FaqCell
        if !isShow[pos] {
            isShow[pos] = true
            tableconst.constant += ceil(cell.contentLB.isHeight() + 90.0)
            print("plllgfg \(tableconst.constant)")
        }else{
            isShow[pos] = false
            tableconst.constant -= ceil(cell.contentLB.isHeight() + 90.0)
            print("pnbvbccvnbnbcvlllgfg \(tableconst.constant)")
        }
        tableView.reloadData()
    }

    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func send_btn(_ sender: Any) {
        guard !titleTF.text!.isEmpty else {
            self.toast("Enter Message Title")
            return
        }
        guard !messageTF.text!.isEmpty else {
                 self.toast("Enter Message Content")
                 return
             }
        let params:[String:Any] = [
            "member_id":dev.string(forKey: "id") ?? "",
            "title":titleTF.text!,
            "body":messageTF.text!
        ]
        
        if self.is_login(){
            BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: helpURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200 ,let data = result,data["success"].boolValue{
                self.toast("Sent Successfully")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                self.toast("internet")
            }
        }
            
        }else{
             self.toast("Sign In Please")
        }
    }
}
