

import UIKit
import SwiftyJSON
class NotificationsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var nodataLB: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backB: UIButton!
    var notis = [JSON]()
    let dev = UserDefaults.standard
    override func viewWillAppear(_ animated: Bool) {
        getNoti()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        
    }
    func getNoti(){
        tableView.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        tableView.delegate = self
        tableView.dataSource = self
        if self.is_login(){
            self.notis.removeAll()
            BGLoading.instance.showLoading(view)
            Helper_API.instance.postActionWithAuth(url: NotificationsListURL, params: ["member_id" : dev.string(forKey: "id") ?? ""]) { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200,let data = result,data["success"].boolValue,data["data"].arrayValue.count > 0 {
                    self.tableView.isHidden = false
                    for item in data["data"].arrayValue{
                        self.notis.append(item["Notification"])
                    }
                    self.tableView.reloadData()
                }else if code == 0{
                    self.nodataLB.text = "No Internet Connnection \n Please Check Your Connection And Reload.".localized()
                    self.nodataLB.setLineHeight(lineHeight: 1.7)
                    self.tableView.isHidden = true
                }else if code == 401{
                    self.sessionExpired()
                } else{
                    self.tableView.isHidden = true
                }
            }
        }else{
            self.tableView.isHidden = true
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as? NotificationCell,notis.count > 0{
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = notis[pos]
            cell.nameLB.text = item["text"].stringValue
            cell.bodyLB.text = item["body"].stringValue
            cell.bodyLB.setLineHeight(lineHeight: 1.5)
            if item["is_read"].boolValue{
                cell.bgv.backgroundColor = UIColor(named: "NotificationColor")
            }else{
                cell.bgv.backgroundColor = UIColor(named: "bgvColor")
            }
            return cell
        }else{
            return UITableViewCell()
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pos = indexPath.row
        let item = notis[pos]
        let par = ["id":item["id"].stringValue]
        if item["is_read"].boolValue{
            if item["type"].stringValue == "reservation" {
                let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UpcomingReservationDetailsVC") as! UpcomingReservationDetailsVC
                vc.reservation_id = item["reservation_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
            }else if item["type"].stringValue == "group"{
                let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                vc.group_id = item["collection_id"].stringValue
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }else{
            BGLoading.instance.showLoading(view)
            Helper_API.instance.postActionWithAuth(url: readNotiURL, params: par) { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200,let data = result,data["success"].boolValue{
                    if item["type"].stringValue == "reservation" {
                        let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UpcomingReservationDetailsVC") as! UpcomingReservationDetailsVC
                        vc.reservation_id = item["reservation_id"].stringValue
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else if item["type"].stringValue == "group"{
                        let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                        vc.group_id = item["collection_id"].stringValue
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }else{
                    self.toast("internet")
                }
            }
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    @IBAction func refreshData_btn(_ sender: Any) {
        getNoti()
    }
    
    
    
    
}
