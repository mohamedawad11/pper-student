

import UIKit
import WebKit
class PaymentVC: UIViewController,WKNavigationDelegate {
    @IBOutlet weak var backB: UIButton!
    var url = ""
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        print("uuuu \(url)")
        let urlString = URL(string: self.url)
        let request = URLRequest(url: urlString!)
        webView.navigationDelegate = self
        webView.load(request)
    }
     //MARK:- WKNavigationDelegate Methods

        //Equivalent of shouldStartLoadWithRequest:
        func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
            var action: WKNavigationActionPolicy?

            defer {
                decisionHandler(action ?? .allow)
            }

            guard let url = navigationAction.request.url else { return }
            print("decidePolicyFor - url: \(url)")
   
      
        }

        //Equivalent of webViewDidStartLoad:
        func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
            print("didStartProvisionalNavigation - webView.url: \(String(describing: webView.url?.description))")
        }

        //Equivalent of didFailLoadWithError:
        func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
            let nserror = error as NSError
            if nserror.code != NSURLErrorCancelled {
                webView.loadHTMLString("Page Not Found", baseURL: URL(string: "https://developer.apple.com/"))
            }
        }

        //Equivalent of webViewDidFinishLoad:
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            print("didFinish - webView.url: \(String(describing: webView.url?.description))")

        }

    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

