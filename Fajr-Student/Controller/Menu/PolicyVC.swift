//
//  PolicyVC.swift
//  Fajr-Student
//
//  Created by osx on 12/29/20.
//

import UIKit

class PolicyVC:  UIViewController {
    @IBOutlet weak var backB: UIButton!
    
    @IBOutlet weak var policyLB: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
      //  if self.is_login(){
            BGLoading.instance.showLoading(view)
            Helper_API.instance.staticPages(id: "\(self.getLang())/2") { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200 ,let data = result{
                    if data["success"].boolValue{
                    self.policyLB.text = data["data"]["Page"]["body"].stringValue.htmlToString
                    self.policyLB.setLineHeight(lineHeight: 1.5)
                    }else{
                        self.to_ast(data["message"].stringValue)
                    }
                }
            }
        //}
    }
    
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
