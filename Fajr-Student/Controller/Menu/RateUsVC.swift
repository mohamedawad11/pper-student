

import UIKit
import Cosmos
class RateUsVC: UIViewController {
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var commentTF: UITextField!
    
    @IBOutlet weak var rateView: CosmosView!
    var rate = 0.0
    let dev = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        rate = rateView.rating
        rateView.didFinishTouchingCosmos = { rating in
            self.rate = rating
        }

    }
    
    @IBAction func sendBTN(_ sender: Any) {
        let params:[String:Any] = [
            "member_id":dev.string(forKey: "id") ?? "",
            "rate":rate
        ]
        if self.is_login(){ BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: rateURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
                     if code == 200 ,let data = result,data["success"].boolValue{
                         self.toast("Sent Successfully")
                         DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                             self.navigationController?.popViewController(animated: true)
                         }
                     }else{
                         self.toast("internet")
                     }
        }
            
        }else{
            self.toast("Sign In Please")
        }
    }
    
    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
