
import UIKit
import GoogleMaps
import Cosmos
class ReservationDetailsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate{
    @IBOutlet weak var rateView: BGView!
    @IBOutlet weak var rate: CosmosView!
    @IBOutlet weak var hideRateViewB: UIButton!
    
    @IBOutlet weak var commentTV: UITextView!
    
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var collectionNameLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var teacherNameLb: UILabel!
    @IBOutlet weak var timeLb: UILabel!
    @IBOutlet weak var priceLb: UILabel!
    @IBOutlet weak var paymentLb: UILabel!
    @IBOutlet weak var routTo: UILabel!
    @IBOutlet weak var courseStatusLB: UILabel!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableConst: NSLayoutConstraint!
    var lat = 0.0
    var lon = 0.0
    var reservation_id = ""
    let dev = UserDefaults.standard
    var members = [StudentModel]()
    var reservation:ReservationModel?
    override func viewWillAppear(_ animated: Bool) {
        rateView.isHidden = true
        hideRateViewB.isHidden = true
        commentTV.text = "Write a few words about your experience".localized()
        commentTV.textColor = UIColor(named: "LableColor")
        commentTV.delegate = self
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "MemberCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
        tableView.delegate = self
        tableView.dataSource = self
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        getReservation()
        
    }
    func getReservation(){
        let params = [
            "reservation_id":reservation_id,
            "student_id":dev.string(forKey: "id") ?? ""
        ]
        
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuthAwad(url: viewReservationURL, params: params) { code,success, reserv, students in
            BGLoading.instance.dismissLoading()
            if code == 200,success,let res = reserv,let ss = students{
                self.setData(res)
                self.reservation = res
                if res.isRated ?? false{
                    self.rateView.isHidden = true
                    self.hideRateViewB.isHidden = true
                }else{
                    self.rateView.isHidden = false
                    self.hideRateViewB.isHidden = false
                }
                self.members = ss
                self.tableView.reloadData()
                self.tableConst.constant = CGFloat(self.members.count * 44)
            }else if code == 401{
                self.sessionExpired()
            }
        }
    }
    func setData(_ item:ReservationModel){
        collectionNameLb.text = item.collectionName ?? ""
        dateLb.text = item.scheduleDay ?? ""
        teacherNameLb.text = item.teacher ?? ""
        timeLb.text = "\(item.scheduleFrom ?? "") \(item.scheduleTo ?? "")"
        priceLb.text = "\(item.cost ?? "") \(currencyTXT)"
        paymentLb.text = item.paid ?? "" == "yes" ? "Paid".localized() : "Not Paid".localized()
        routTo.text = "\("Route to".localized()) \(item.teacher ?? "")"

        lat = ((item.lat ?? "") as NSString).doubleValue
        lon = ((item.lon ?? "") as NSString).doubleValue
        courseStatusLB.text = item.status ?? ""
        setToMap()
    }
    func setToMap(){
        let markerSquirt = GMSMarker()
        markerSquirt.position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        markerSquirt.icon = UIImage(named: "marker")
        markerSquirt.map = viewMap
        viewMap.camera = GMSCameraPosition(latitude: lat, longitude: lon, zoom: 15.0, bearing: 6, viewingAngle: 100)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as? MemberCell,members.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = members[pos]
            cell.name.text = "\(item.fName ?? "") \(item.lName ?? "")"
            return cell
        }else{
            return UITableViewCell()
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text! == "Write a few words about your experience".localized(){
            textView.text.removeAll()
            textView.textColor = UIColor(named: "secondryColor")
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
          if textView.text! == ""{
                  textView.text = "Write a few words about your experience".localized()
                  textView.textColor = UIColor(named: "LableColor")
              }
    }
    
    @IBAction func sendRateBTN(_ sender: Any) {
        guard commentTV.text! != "Write a few words about your experience".localized() else {
            self.toast("Enter Comment")
            return
        }
        if let item = reservation{
            let params:[String:Any] = [
                "reservation_id":item.id ?? "",
                "review_body":commentTV.text!,
                "rate":rate.rating,
                "student_id":dev.string(forKey: "id") ?? ""
            ]
            BGLoading.instance.showLoading(view)
            Helper_API.instance.postActionWithAuth(url: rateReservationURL, params: params) { (code, result) in
               BGLoading.instance.dismissLoading()
                if code == 200,let data = result,data["success"].boolValue{
                    UIView.animate(withDuration: 0.2) {
                        self.rateView.isHidden = true
                        self.hideRateViewB.isHidden = true
                    }
                }else{
                    self.toast("internet")
                }
            }
        }
    }
    @IBAction func hideRateViewBTN(_ sender: Any) {
        UIView.animate(withDuration: 0.2) {
            self.rateView.isHidden = true
            self.hideRateViewB.isHidden = true
        }
    }
    
    
}
