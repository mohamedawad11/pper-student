

import UIKit
import SwiftyJSON
class ReservationsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource{
    
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var tableConst: NSLayoutConstraint!
    @IBOutlet weak var previousReloadView: UIView!
    @IBOutlet weak var upcomingReloadView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    var upcomingReserv = [JSON]()
    var previousReserv = [JSON]()
    let dev = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{backB.setImage(UIImage(named: "ll"), for: .normal)}
        tableView.register(UINib(nibName: "PreviousReservCell", bundle: nil), forCellReuseIdentifier: "PreviousReservCell")
        tableView.delegate = self
        tableView.dataSource = self
        reloadData("prev")
        
        collectionView.register(UINib(nibName: "UpcomingReservCell", bundle: nil), forCellWithReuseIdentifier: "UpcomingReservCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.setCollectionLayOut(110, 1)
        reloadData("upcoming")
    }
    func reloadData(_ status:String){
        let params:[String:Any] = [
            "student_id":dev.string(forKey: "id") ?? "0",
            "status":status,
            "limit":20,
            "offset":0
        ]
        print("pppppppp \(params)")
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: studentReservationsURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200 ,let data = result,data["success"].boolValue{
                switch status {
                case "prev":
                    if data["success"].boolValue{
                        for item in data["data"].arrayValue{
                            self.previousReserv.append(item["Reservation"])
                        }
                        if self.previousReserv.count > 0 {
                            self.tableView.isHidden = false
                            self.previousReloadView.isHidden = true
                        }else{
                            self.tableView.isHidden = true
                            self.previousReloadView.isHidden = false
                        }
                        self.tableView.reloadData()
                        self.tableConst.constant = CGFloat(self.previousReserv.count * 85)
                    }else{
                        self.tableView.isHidden = true
                        self.previousReloadView.isHidden = false
                        // self.to_ast(data["message"].stringValue)
                    }
                case "upcoming":
                    if data["success"].boolValue{
                        for item in data["data"].arrayValue{
                            self.upcomingReserv.append(item["Reservation"])
                        }
                        if self.upcomingReserv.count > 0 {
                            self.upcomingReloadView.isHidden = true
                            self.collectionView.isHidden = false
                        }else{
                            self.upcomingReloadView.isHidden = false
                            self.collectionView.isHidden = true
                        }
                        self.collectionView.reloadData()
                    }else{
                        self.upcomingReloadView.isHidden = false
                        self.collectionView.isHidden = true
                        //  self.to_ast(data["message"].stringValue)
                    }
                default:
                    print("no thing")
                }
            }else if code == 401{
                self.sessionExpired()
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return upcomingReserv.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpcomingReservCell", for: indexPath) as? UpcomingReservCell,upcomingReserv.count > 0 {
            let pos = indexPath.row
            let item = upcomingReserv[pos]
            cell.collectionNameLb.text = item["level_and_subject_name"].stringValue
            cell.dateLb.text = item["reservation_day"].stringValue
            cell.teacherLb.text = item["teacher_name"].stringValue
            cell.timeLb.text = item["reservation_time"].stringValue
            cell.priceLb.text = item["cost"].stringValue + " " + currencyTXT
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pos = indexPath.row
        let item = upcomingReserv[pos]
        let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "UpcomingReservationDetailsVC") as! UpcomingReservationDetailsVC
        vc.reservation_id = item["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return previousReserv.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PreviousReservCell", for: indexPath) as? PreviousReservCell,previousReserv.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = previousReserv[pos]
            cell.collectionNameLB.text = item["level_and_subject_name"].stringValue
            cell.teacherNameLB.text = item["teacher_name"].stringValue
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pos = indexPath.row
        let item = previousReserv[pos]
        let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "ReservationDetailsVC") as! ReservationDetailsVC
        vc.reservation_id = item["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func reloadUpcoming_btn(_ sender: Any) {
        reloadData("upcoming")
    }
    @IBAction func reloadPrevious_btn(_ sender: Any) {
        reloadData("prev")
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
}
