//
//  SettingsVC.swift
//  Fajr-Student
//
//  Created by osx on 12/29/20.
//

import UIKit

class SettingsVC: UIViewController {
    @IBOutlet weak var logoutLB: UILabel!
    @IBOutlet weak var groupsView: UIView!
    @IBOutlet weak var editAccountView: UIView!
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var back_b: UIButton!
    @IBOutlet weak var phoneLB: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var userNameLB: UILabel!
    let dev = UserDefaults.standard
    override func viewWillAppear(_ animated: Bool) {
        if self.is_login(){
            if let photo = dev.string(forKey: "photo"){
                logo.sd_setImage(with: URL(string: "\(imageURL)\(dev.string(forKey: "id")!)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
            }
            if let f_name = dev.string(forKey: "f_name"),let l_name = dev.string(forKey: "l_name"){
                userNameLB.text = f_name + " " + l_name
            }
            if let phone = dev.string(forKey: "phone"){
                phoneLB.text = phone
            }
            
        }else{
            logoutLB.text = "Sign In".localized()
            userInfoView.isHidden = true
            editAccountView.isHidden = true
            groupsView.isHidden = true
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.is_ar() == "ar"{
            back_b.setImage(UIImage(named: "ll"), for: .normal)
        }
        
        
    }
    @IBAction func back_btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func action_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            guard self.is_login() else {
                self.login()
                return
            }
            print("edit profile")
            self.navigationController?.pushViewController(UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "EditAccountVC"), animated: false)
        case 1:
            guard self.is_login() else {
                self.login()
                return
            }
            print("Groups")
            self.navigationController?.pushViewController(UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupListVC"), animated: false)
        //
        case 2:
            
            print("About App")
            self.navigationController?.pushViewController(UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "AboutVC"), animated: false)
        case 3:
            guard self.is_login() else {
                self.login()
                return
            }
            print("Contact Us")
            self.navigationController?.pushViewController(UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "ContactUsVC"), animated: false)
        case 4:
            guard self.is_login() else {
                self.login()
                return
            }
            print("Help")
            self.navigationController?.pushViewController(UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "HelpVC"), animated: false)
            
        case 5:
           
            print("Privacy Policy")
            self.navigationController?.pushViewController(UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "PolicyVC"), animated: false)
            
        case 6:
            print("Rate us")
            self.rateApp(appId: "id1549472942")
//            self.navigationController?.pushViewController(UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "RateUsVC"), animated: false)
        case 7:
            print("logout")
            guard self.is_login() else {
                self.login()
                return
            }
            logoutFunc(sender)
        default:
            print("noThing")
        }
    }
    
    func logoutFunc(_ sender:UIButton){
        let alertController = UIAlertController(title: self.is_login() ? "Do You Want To Logout?!".localized() : "Do You Want To Sign In?!".localized(), message: nil, preferredStyle: .actionSheet)
        let OKAction = UIAlertAction(title: "Yes".localized(), style: .default, handler: { action in
            if self.is_login(){
                BGLoading.instance.showLoading(self.view)
                Helper_API.instance.postActionWithAuth(url: logOutURL, params: ["member_id" : self.dev.string(forKey: "id") ?? ""]) { (code, result) in
                    BGLoading.instance.dismissLoading()
                    if code == 200,let data = result,data["success"].boolValue{
                        self.dev.set(false, forKey: "is_login")
                        self.login()
                    }else{
                        self.dev.set(false, forKey: "is_login")
                        self.login()
                    }
                }
            }else{
                self.login()
            }
        })
        alertController.addAction(OKAction)
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        if UIDevice.current.userInterfaceIdiom == .pad {
            alertController.popoverPresentationController?.sourceView = sender
            alertController.popoverPresentationController?.sourceRect = sender.bounds
            alertController.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        
        self.present(alertController, animated: true)
    }
    
}
