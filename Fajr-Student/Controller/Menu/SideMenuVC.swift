

import UIKit
import SwiftyJSON
class SideMenuVC: UIViewController {
    @IBOutlet weak var notification_B: buttonWithBadge!
    @IBOutlet weak var studentWallet_LB: UILabel!
    @IBOutlet weak var wishTxt: UILabel!
    @IBOutlet weak var wishView: UIView!
    @IBOutlet weak var userview: UIView!
    @IBOutlet weak var reservationsView: UIView!
    @IBOutlet weak var WalletView: UIView!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var phoneLB: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var userNameLB: UILabel!
    let dev = UserDefaults.standard
    override func viewWillAppear(_ animated: Bool) {
        wishTxt.setLineHeight(lineHeight: 1)
        if self.is_login(){
            getStudentWallet()
            if let photo = dev.string(forKey: "photo"){
                logo.sd_setImage(with: URL(string: "\(imageURL)\(dev.string(forKey: "id")!)/original/\(photo)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!), placeholderImage: #imageLiteral(resourceName: "profilePlaceHolder"), completed: nil)
            }
            if let f_name = dev.string(forKey: "f_name"),let l_name = dev.string(forKey: "l_name"){
                userNameLB.text = f_name + " " + l_name
            }
            if let phone = dev.string(forKey: "phone"){
                phoneLB.text = phone
            }
            
        }else{
            userview.isHidden = true
            reservationsView.isHidden = true
            WalletView.isHidden = true
            chatView.isHidden = true
        }
    }
    
    func getStudentWallet(){
        let par = ["student_id":dev.string(forKey: "id") ?? ""]
        Helper_API.instance.postActionWithAuth(url: getStudentWalletURL, params: par) { (code, result) in
            if code == 200,let data = result,data["success"].boolValue{
                self.studentWallet_LB.text = "\(data["data"].intValue)"
            }
        }
        Helper_API.instance.notReadNoti { (code, result) in
            if code == 200 ,let data = result,data["success"].boolValue{
                self.notification_B.showBadge(withCount: data["count"].intValue)
            }else if code == 401{
                self.sessionExpired()
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketIOManager.sharedInstance.socket.on("app_update_member_notification") { (data, ack) in
            self.notification_B.showBadge(withCount: JSON(data)[0]["count"].intValue)
             }
        SocketIOManager.sharedInstance.socket.on("app_student_walet_update") { (data, ack) in
            self.studentWallet_LB.text = "\(JSON(data)[0]["count"].intValue)"
                    }
    }
    @IBAction func actionsBTN(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            guard self.is_login() else {
                self.login()
                return
            }
            print("reservations")
            self.navigationController?.pushViewController(UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "ReservationsVC"), animated: false)
        case 1:
            guard self.is_login() else {
                self.login()
                return
            }
            print("Wallet")
            let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "WalletVC") as! WalletVC
            vc.wallettxt = studentWallet_LB.text!
            self.navigationController?.pushViewController(vc, animated: false)
        case 2:
            guard self.is_login() else {
                self.login()
                return
            }
            print("Chat")
              self.navigationController?.pushViewController(UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "ChatListVC"), animated: false)
            //
        case 3:
            guard self.is_login() else {
                self.login()
                return
            }
            print("Notifications")
              self.navigationController?.pushViewController(UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "NotificationsVC"), animated: false)
            
        case 4:
            print("Settings")
            self.navigationController?.pushViewController(UIStoryboard(name: "SideMenu", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC"), animated: false)
        default:
            print("noThing")
        }
    }
    
    @IBAction func signUpAsTeacherBTN(_ sender: Any) {
        let instagramHooks = "PPerTeacherApp://"
        let instagramUrl = URL(string: instagramHooks)!
        if UIApplication.shared.canOpenURL(instagramUrl)
        {
            UIApplication.shared.open(instagramUrl)
        } else {
            self.rateApp(appId: "id1545844231")
        }
    }
    
}
