

import UIKit
import GoogleMaps
import Cosmos
import MyFawryPlugin
class UpcomingReservationDetailsVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate{
    @IBOutlet weak var creditCardBTN: UIView!
    @IBOutlet weak var paymentStackView: UIStackView!
    @IBOutlet weak var icCredit: UIImageView!
    @IBOutlet weak var cashValueLB: UILabel!
    @IBOutlet weak var icCash: UIImageView!
    
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var collectionNameLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var teacherNameLb: UILabel!
    @IBOutlet weak var timeLb: UILabel!
    @IBOutlet weak var priceLb: UILabel!
    @IBOutlet weak var paymentLb: UILabel!
    @IBOutlet weak var routTo: UILabel!
    @IBOutlet weak var courseStatusLB: UILabel!
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableConst: NSLayoutConstraint!
    var lat = 0.0
    var lon = 0.0
    var reservation_id = ""
    let dev = UserDefaults.standard
    var members = [StudentModel]()
    var reservation:ReservationModel?
    
    
    let FawryPlugin = Fawry.sharedInstance
    override func viewWillAppear(_ animated: Bool) {
        
        creditCardBTN.isHidden = CountrySymbol == "EG" ? true:false
        tableView.register(UINib(nibName: "MemberCell", bundle: nil), forCellReuseIdentifier: "MemberCell")
        tableView.delegate = self
        tableView.dataSource = self
        getReservation()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        FawryPlugin?.paymentOperationSuccess(onViewController: self) { (transactionID, statusCode) in
            if transactionID != nil {
                print("Transaction ID = \(transactionID ?? "" as AnyObject)")
            }
        }
        FawryPlugin?.paymentOperationFailure(onViewController: self) { (transactionID, statusCode) in
            print("kjgjkfjkgjkfg \(transactionID)")
        print("payment Operation Failure")
        }
    }
    func getReservation(){
        let params = [
            "reservation_id":reservation_id,
            "student_id":dev.string(forKey: "id") ?? ""
        ]
        
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuthAwad(url: viewReservationURL, params: params) { code,success, reserv, students in
            BGLoading.instance.dismissLoading()
            if code == 200,success,let res = reserv,let ss = students{
                self.setData(res)
                self.reservation = res
                self.cashValueLB.text = "\("Cash   Will Pay ".localized()) \(res.willPay ?? 0) \(currencyTXT)"
                if res.paid ?? "" == "no"{
                    self.paymentStackView.isHidden = false
                }else{
                    self.paymentStackView.isHidden = true
                }
                self.members = ss
                self.tableView.reloadData()
                self.tableConst.constant = CGFloat(self.members.count * 44)
            }else if code == 401{
                self.sessionExpired()
            }
        }
    }
    func setData(_ item:ReservationModel){
        collectionNameLb.text = item.collectionName ?? ""
        dateLb.text = item.scheduleDay ?? ""
        teacherNameLb.text = item.teacher ?? ""
        timeLb.text = "\(item.scheduleFrom ?? "") \(item.scheduleTo ?? "")"
        priceLb.text = item.cost ?? "" + " " + currencyTXT
        paymentLb.text = item.paid ?? "" == "yes" ? "Paid".localized() : "Not Paid".localized()
        routTo.text = "\("Route to".localized()) \(item.teacher ?? "")"
        
        lat = ((item.lat ?? "") as NSString).doubleValue
        lon = ((item.lon ?? "") as NSString).doubleValue
        courseStatusLB.text = item.status ?? ""
        setToMap()
    }
    func setToMap(){
        let markerSquirt = GMSMarker()
        markerSquirt.position = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        markerSquirt.icon = UIImage(named: "marker")
        markerSquirt.map = viewMap
        viewMap.camera = GMSCameraPosition(latitude: lat, longitude: lon, zoom: 15.0, bearing: 6, viewingAngle: 100)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return members.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MemberCell", for: indexPath) as? MemberCell,members.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = members[pos]
            cell.name.text = "\(item.fName ?? "") \(item.lName ?? "")"
            return cell
        }else{
            return UITableViewCell()
        }
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func payBTNAction(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            icCash.image = UIImage(named: "c2")
            icCash.tintColor = UIColor(named: Colors.instance.mainColor)
            icCredit.image = UIImage(named: "c1")
            icCredit.tintColor = UIColor(named: Colors.instance.grayColor)
        case 1:
            icCredit.image = UIImage(named: "c2")
            icCredit.tintColor = UIColor(named: Colors.instance.mainColor)
            icCash.image = UIImage(named: "c1")
            icCash.tintColor = UIColor(named: Colors.instance.grayColor)
            if let item = reservation{
                let par:[String:Any] = [
                    "amount":item.willPay ?? 0,
                    "reservation_id":item.id ?? "",
                    "studentId":dev.string(forKey: "id") ?? "",
                    "invoiceId":item.invoiceID ?? ""
                ]
//                BGLoading.instance.showLoading(view)
//                Helper_API.instance.payment(url: getBankURL, params: par) { (code, result) in
//                    BGLoading.instance.dismissLoading()
//                    print("djfjdjjfdhjg \(result!)")
//                    if let data = result{
//                        if data["success"].boolValue{
//                            let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
//                            vc.url = data["data"].stringValue
//                            self.navigationController?.pushViewController(vc, animated: true)
//                        }else{
//                            self.to_ast(data["data"].stringValue)
//                        }
//                    }else{
//                        self.toast("internet")
//                    }
//                }
                var itemsList = [Item]()
                itemsList.append(Item(productSKU: "", productDescription: "", quantity: 1, price: Double(item.willPay ?? 0), originalPrice: Double(item.willPay ?? 0), width: 1, height: 1, length: 1, weight: 1, variantCode: "", reservationCodes: nil, earningRuleId: nil))
                
                FawryPlugin?.customerMobileNumber = "01234567890"
                FawryPlugin?.customerEmailAddress = "y.monem@pperapp.com"
                FawryPlugin?.customerName = "y.monem@pperapp.com"
                FawryPlugin?.skipCustomerInput = true

//                FawryPlugin?.initialize(serverURL: "https://www.atfawry.com/", styleParam: .none, merchantIDParam: "siYxylRjSPyu/K+KSUwRRw==", merchantRefNum: "cd72e51cc5be4c73b448dcd7a9bcb470", languageParam: "ar", GUIDParam: "#@hjfdjhfdhjhj", customeParameterParam: nil, currancyParam: .EGP, items: itemsList)
                FawryPlugin?.initialize(serverURL: "https://www.atfawry.com/", styleParam: .none, merchantIDParam: "siYxylRjSPyu/K+KSUwRRw==", merchantRefNum: "cd72e51cc5be4c73b448dcd7a9bcb470", languageParam: "ar", GUIDParam: "#@DDGFGGFTRT", customeParameterParam: nil, currancyParam: .EGP, items: itemsList, orderExpiryInHours: 5, secureKey: "cd72e51cc5be4c73b448dcd7a9bcb470", signature: nil)
                
                FawryPlugin?.showSDK(onViewController: self) { (transactionID, statusCode) in
                    if transactionID != nil {
                        print("mnvcmnbnmmnbnvnmb")
                    }
                }
            }
            
        default:
            print("no thing")
        }
    }
    
}
