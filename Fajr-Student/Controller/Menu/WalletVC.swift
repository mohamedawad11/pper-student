

import UIKit
import SwiftyJSON
class WalletVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var backB: UIButton!
    var trans = [JSON]()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var amountLB: UILabel!
    
    @IBOutlet weak var tableConst: NSLayoutConstraint!
    var wallettxt = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        amountLB.text = wallettxt
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        tableView.register(UINib(nibName: "TransactionCell", bundle: nil), forCellReuseIdentifier: "TransactionCell")
        tableView.delegate = self
        tableView.dataSource = self
        BGLoading.instance.showLoading(view)
        Helper_API.instance.transactions { (code, result) in
          BGLoading.instance.dismissLoading()
            if code == 200,let data = result,data["success"].boolValue{
                    for item in data["data"].arrayValue{
                        self.trans.append(item["Walet"])
                    }
                    self.tableView.reloadData()
                    self.tableConst.constant = CGFloat(self.trans.count * 106)
            }else if code == 401{
                self.sessionExpired()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as? TransactionCell,trans.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = trans[pos]
            cell.teachernameLB.text = item["teacher"].stringValue
            cell.priceLB.text = item["reservation_price"].stringValue + " " + currencyTXT
            let day = item["schedule_day"].stringValue
            let from = self.getDateFromString("HH:mm:ss", item["schedule_from"].stringValue, "hh:mm a")
            let to = self.getDateFromString("HH:mm:ss", item["schedule_to"].stringValue, "h:mm a")
            let created = self.getDateFromString("yyyy-MM-dd HH:mm:ss", item["created"].stringValue, "yyyy-MM-dd / HH:mm a")
            cell.scheduleDayLB.text = "\(day) / \(from)-\(to)"
            cell.createdLB.text = created
            cell.amountLB.text = "\((item["amount"].stringValue as NSString).doubleValue)" + " " + "SAR".localized()
            if (item["amount"].stringValue as NSString).doubleValue > 0 {
                cell.bgv.backgroundColor = UIColor(hexString: "E5EDF2")
            }else{
                cell.bgv.backgroundColor = UIColor(hexString: "FFF2F0")
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pos = indexPath.row
            let item = trans[pos]
            let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "ReservationDetailsVC") as! ReservationDetailsVC
            vc.reservation_id = item["reservation_id"].stringValue
            self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
   
}
