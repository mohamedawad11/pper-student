
import UIKit
import SwiftyJSON
class AddGroupVC: UIViewController ,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource{
  
    @IBOutlet weak var collectionConst: NSLayoutConstraint!
    
    
    @IBOutlet weak var addMember_btn: UIButton!
    
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var groupNameLB: UITextField!
    var students = [JSON]()
    let dev = UserDefaults.standard
    var level_id = ""
    var members = [JSON]()
    var subjects = [JSON]()
    var subjectId = "-1"
      var subjects_names = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionview.register(UINib(nibName: "GroupMemberCell", bundle: nil), forCellWithReuseIdentifier: "GroupMemberCell")
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.setCollectionWithOutHorizontalLayOut(38, 3)
        level_id = dev.string(forKey: "student_level_id") ?? ""
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
        tableView.register(UINib(nibName: "StudentCell", bundle: nil), forCellReuseIdentifier: "StudentCell")
        tableView.delegate = self
        tableView.dataSource = self
        searchView.isHidden = true
        searchImage.isHidden = true
        collectionConst.constant = 0.0
        getSubjects()
    }
    
    func getSubjects(){
        Helper_API.instance.levels { (code, result) in
            if code == 200,let data = result {
                if data["success"].boolValue{
                    self.subjects = data["data"].arrayValue
                    for item in data["data"].arrayValue{
                        self.subjects_names.append(item["name"].stringValue)
                    }
                }else{
                    self.to_ast(data["data"]["message"].stringValue)
                }
            }else{
                self.toast("internet")
            }
        }
    }
    @IBAction func selectLevel_btn(_ sender: UIButton) {
        self.drop(anchor: sender, dataSource: subjects_names) { (item, index) in
            sender.setTitle(item, for: .normal)
            sender.setTitleColor(UIColor(named: "primaryColor"), for: .normal)
            self.subjectId = self.subjects[index]["id"].stringValue
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return members.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupMemberCell", for: indexPath) as? GroupMemberCell,members.count > 0 {
            let pos = indexPath.row
            let item = members[pos]
            cell.nameLB.text = item["f_name"].stringValue + " " + item["l_name"].stringValue
            cell.removeBTN.tag = pos
            cell.removeBTN.addTarget(self, action: #selector(removeMemberTapped), for: .touchUpInside)
            return cell
        }else{
            return UICollectionViewCell()
        }
      }
    @objc func removeMemberTapped(_ sender:UIButton){
        let pos = sender.tag
        members.remove(at: pos)
        collectionview.reloadData()
        collectionConst.constant = CGFloat((members.count % 3) == 0 ? (members.count / 3) * 48:((members.count / 3) * 48) + 48)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as? StudentCell,students.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = students[pos]
            let f_name = item["f_name"].stringValue
            let l_name = item["l_name"].stringValue
            cell.name.text = f_name + " " + l_name
            return cell
        }else{
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pos = indexPath.row
        let item = students[pos]
        members.append(item)
        members = members.unique
        collectionview.reloadData()
        collectionConst.constant = CGFloat((members.count % 3) == 0 ? (members.count / 3) * 48:((members.count / 3) * 48) + 48)
        UIView.animate(withDuration: 0.2) {
            self.searchImage.isHidden = true
            self.searchView.isHidden = true
        }
    }
    
    @IBAction func searchForStudent_btn(_ sender: Any) {
        self.students.removeAll()
        let params = [
            "student_id":dev.string(forKey: "id") ?? "",
            "level_id":level_id,
            "mobile_number":phoneTF.text!.convertToEnglish()
        ]
        Helper_API.instance.postActionWithAuth(url: getStudentsByMobileURL, params: params) { (code, result) in
            if code == 200,let data = result,data["success"].boolValue{
                for item in data["data"].arrayValue{
                    self.students.append(item["Member"])
                }
                if self.students.count > 0 {
                    self.searchView.isHidden = false
                    self.searchImage.isHidden = false
                }else{
                    self.toast("No students for this phone number")

//                    self.searchView.isHidden = true
//                    self.searchImage.isHidden = true
                }
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func createGroup_btn(_ sender: Any) {
        guard !groupNameLB.text!.isEmpty else {
            self.toast("Enter Group Name")
            return
        }
        guard subjectId != "-1" else {
            self.toast("Select Group Subject")
            return
        }
        guard members.count != 0 else {
            self.toast("Add Member")
            return
        }
        var students = [[String:String]]()
        for item in members{
            students.append(["student_id":item["id"].stringValue])
        }
        let params:[String:Any] = [
            "studentId":dev.string(forKey: "id") ?? "",
            "subjectId":subjectId,
            "LevelId":level_id,
            "groupName":groupNameLB.text!,
            "students":students
        ]
        BGLoading.instance.showLoading(view)
        Helper_API.instance.addGroup(url: addGroupURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200,let data = result,data["success"].boolValue{
                self.toast("Group Added Successfully")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                self.toast("internet")
            }
        }
        
    }
    


    @IBAction func backBTN(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addStudent_btn(_ sender: Any) {
        searchImage.isHidden = false
        searchView.isHidden = false
    }
    @IBAction func cancelMemberView_btn(_ sender: Any) {
        searchImage.isHidden = true
        UIView.animate(withDuration: 0.2) {
            self.searchView.isHidden = true
        }
    }
}
