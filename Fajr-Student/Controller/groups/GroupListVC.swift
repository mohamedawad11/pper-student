
import UIKit
import SwiftyJSON
class GroupListVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
  
    
    @IBOutlet weak var backB: UIButton!
    @IBOutlet weak var tableView: UITableView!
    let dev = UserDefaults.standard
    var groups = [JSON]()
    override func viewWillAppear(_ animated: Bool) {
        tableView.register(UINib(nibName: "StudentGroupsCell", bundle: nil), forCellReuseIdentifier: "StudentGroupsCell")
               tableView.delegate = self
               tableView.dataSource = self
        self.groups.removeAll()
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: studentGroupsURL, params: ["student_id" : dev.string(forKey: "id") ?? ""]) { (code, result) in
             BGLoading.instance.dismissLoading()
            if code == 200,let data = result,data["success"].boolValue,data["data"].arrayValue.count > 0 {
                for item in data["data"].arrayValue{
                    self.groups.append(item["Collection"])
                }
                self.tableView.reloadData()
            }else if code == 401{
                self.sessionExpired()
            } else{
                self.tableView.isHidden = true
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "StudentGroupsCell", for: indexPath) as? StudentGroupsCell,groups.count > 0 {
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            let pos = indexPath.row
            let item = groups[pos]
            cell.name.text = item["collection_name"].stringValue
            return cell
        }else{
            return UITableViewCell()
        }
      }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pos = indexPath.row
        let item = groups[pos]
        let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
        vc.group_id = item["id"].stringValue
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func back_btn(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addGroup_btn(_ sender: UIButton) {
        self.navigationController?.pushViewController(UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "AddGroupVC"), animated: false)
    }
    

}
