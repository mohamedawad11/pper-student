
import UIKit
import SwiftyJSON
class GroupVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{
   
    @IBOutlet weak var studentsTableView: UITableView!
    @IBOutlet weak var addmemberB: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchImage: UIImageView!
    @IBOutlet weak var phoneTF: UITextField!

    @IBOutlet weak var flushB: UIButton!
    @IBOutlet weak var gStack: UIStackView!
    @IBOutlet weak var groupNameLB: UILabel!
    @IBOutlet weak var openChat_b: UIButton!
    @IBOutlet weak var accept_b: UIButton!
    @IBOutlet weak var tableConst: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backB: UIButton!
    var group_id = ""
    let dev = UserDefaults.standard
    var members = [JSON]()
    var group:JSON?
    var students = [JSON]()
    override func viewWillAppear(_ animated: Bool) {
        studentsTableView.register(UINib(nibName: "StudentCell", bundle: nil), forCellReuseIdentifier: "StudentCell")
        studentsTableView.delegate = self
        studentsTableView.dataSource = self
        searchView.isHidden = true
        searchImage.isHidden = true
        
        self.accept_b.isHidden = true
        self.openChat_b.isHidden = true
        self.flushB.isHidden = true
        gStack.isHidden = true
        tableView.isHidden = true
        tableView.register(UINib(nibName: "MemberGroupCell", bundle: nil), forCellReuseIdentifier: "MemberGroupCell")
              tableView.delegate = self
              tableView.dataSource = self
              getData()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.is_ar() == "ar"{
            backB.setImage(UIImage(named: "ll"), for: .normal)
        }
      
    }
    func getData(){
        BGLoading.instance.showLoading(view)
              Helper_API.instance.postActionWithAuth(url: studentViewGroupURL, params: ["group_id" : group_id,"student_id":dev.string(forKey: "id") ?? ""]) { (code, result) in
               BGLoading.instance.dismissLoading()
                  if code == 200,let data = result,data["success"].boolValue{
                    self.group = data["data"]
                      let item = data["data"]["Collection"]
                      self.gStack.isHidden = false
                      self.groupNameLB.text = item["collection_name"].stringValue
                    if !item["belongsToMe"].boolValue,data["data"]["flags"]["acceptButton"].intValue != 0{
                        self.accept_b.isHidden = false
                    }else{
                        self.accept_b.isHidden = true
                    }
                    if item["open_Chat"].boolValue{
                        self.openChat_b.isHidden = false
                    }else{
                        self.openChat_b.isHidden = true
                    }
                    
                    if item["belongsToMe"].boolValue,data["data"]["flags"]["flushButton"].intValue == 1{
                        self.flushB.isHidden = false
                    }else{
                        self.flushB.isHidden = true
                    }
                    if item["belongsToMe"].boolValue{
                        self.addmemberB.isHidden = false
                    }else{
                       self.addmemberB.isHidden = true
                    }
                    self.members = data["data"]["StudentsCollection"].arrayValue
                      self.tableView.isHidden = false
                      self.tableView.reloadData()
                      self.tableConst.constant = CGFloat(self.members.count * 50)
                  }else{
                      self.toast("internet")
                  }
              }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView.tag{
        case 0:
            return 50.0
        case 1:
            return 44.0
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case 0:
            return members.count
        case 1:
            return students.count
        default:
            return 0
        }
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "MemberGroupCell", for: indexPath) as? MemberGroupCell,members.count > 0 {
                      cell.selectionStyle = UITableViewCell.SelectionStyle.none
                      let pos = indexPath.row
                      let item = members[pos]
                      cell.name.text = item["name"].stringValue
                      if let data = group{
                          if data["Collection"]["belongsToMe"].boolValue{
                              cell.removeBTN.isHidden = false
                              if item["student_confirmed"].boolValue{
                                 cell.img.isHidden = true
                              }else{
                                  cell.img.isHidden = false
                              }
                          }else{
                              cell.removeBTN.isHidden = true
                              cell.img.isHidden = true
                          }
                      }
                      cell.removeBTN.tag = pos
                      cell.removeBTN.addTarget(self, action: #selector(removeMemberFromGroupTapped), for: .touchUpInside)
                      return cell
                  }else{
                      return UITableViewCell()
                  }
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as? StudentCell,students.count > 0 {
                      cell.selectionStyle = UITableViewCell.SelectionStyle.none
                      let pos = indexPath.row
                      let item = students[pos]
                      let f_name = item["f_name"].stringValue
                      let l_name = item["l_name"].stringValue
                      cell.name.text = f_name + " " + l_name
                      return cell
                  }else{
                      return UITableViewCell()
                  }
        default:
            return UITableViewCell()
        }
      
       }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 1:
            let student = students[indexPath.row]
            if let item = group{
                let params:[String:Any] = [
                    "ownerId":dev.string(forKey: "id") ?? "",
                    "groupName":item["Collection"]["collection_name"].stringValue,
                    "collection_id":item["Collection"]["id"].stringValue,
                    "student_id":student["id"].stringValue
                ]
                BGLoading.instance.showLoading(view)
                Helper_API.instance.postActionWithAuth(url: addStudentToGroupURL, params: params) { (code, result) in
                   BGLoading.instance.dismissLoading()
                    if code == 200,let data = result,data["success"].boolValue{
                        self.searchView.isHidden = true
                        self.searchImage.isHidden = true
                        self.getData()
                    }else{
                        self.toast("internet")
                    }
                }
            }
        default:
            print("no thing")
        }
    }
    @objc func removeMemberFromGroupTapped(_ sender:UIButton){
        let pos = sender.tag
        let item = members[pos]
        let params:[String:Any] = [
            "id":item["id"].stringValue,
            "student_id":item["student_id"].stringValue,
            "owner_id":dev.string(forKey: "id") ?? "",
            "group_name":item["collectionName"].stringValue
        ]
        BGLoading.instance.showLoading(view)
        Helper_API.instance.postActionWithAuth(url: removeStudentFromGroupURL, params: params) { (code, result) in
            BGLoading.instance.dismissLoading()
            if code == 200,let data = result,data["success"].boolValue{
                self.getData()
            }else{
                self.toast("internet")
            }
            
        }
        
    }
    @IBAction func back_btn(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func action_btn(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            BGLoading.instance.showLoading(view)
            Helper_API.instance.postActionWithAuth(url: acceptInivitaionToJointoGroupURL, params: ["id" : group!["flags"]["acceptButton"].intValue]) { (code, result) in
                BGLoading.instance.dismissLoading()
                if code == 200,let data = result{
                    if data["success"].boolValue{
                        UIView.animate(withDuration: 0.5) {
                            self.accept_b.isHidden = true
                          //  self.openChat_b.isHidden = false
                            self.getData()
                        }
                    }else{
                        self.to_ast(data["message"].stringValue)
                    }
                }else{
                    self.toast("internet")
                }
            }
        case 1:
            print("open Chat")
            let vc = UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            vc.group = group!["Collection"]
            self.navigationController?.pushViewController(vc, animated: false)
        case 2:
            print("flush button")
            if let item = group{
            let params = [
                "collection_id":item["Collection"]["id"].stringValue,
                "collection_name":item["Collection"]["collection_name"].stringValue,
                "owner_id":dev.string(forKey: "id") ?? "0"
                ]
                print("flusssss \(params)")
                BGLoading.instance.showLoading(view)
                Helper_API.instance.postActionWithAuth(url: flushGroupURL, params: params) { (code, result) in
                    BGLoading.instance.dismissLoading()
                    if code == 200,let data = result{
                        if data["success"].boolValue{
                            self.getData()
                        }else{
                            self.to_ast(data["message"].stringValue)
                        }
                    }else{
                        self.toast("internet")
                    }
                }
            }
            
        default:
            print("no thong")
        }
    }
   
    
    @IBAction func addStudent_btn(_ sender: Any) {
          searchImage.isHidden = false
          searchView.isHidden = false
      }
      @IBAction func cancelMemberView_btn(_ sender: Any) {
          searchImage.isHidden = true
          UIView.animate(withDuration: 0.2) {
              self.searchView.isHidden = true
          }
      }
    
    @IBAction func searchForStudent_btn(_ sender: Any) {
        self.students.removeAll()
           let params = [
               "student_id":dev.string(forKey: "id") ?? "",
               "level_id":dev.string(forKey: "student_level_id") ?? "",
               "mobile_number":phoneTF.text!.convertToEnglish()
           ]
           Helper_API.instance.postActionWithAuth(url: getStudentsByMobileURL, params: params) { (code, result) in
               if code == 200,let data = result,data["success"].boolValue{
                   for item in data["data"].arrayValue{
                       self.students.append(item["Member"])
                   }
                   if self.students.count > 0 {
                       self.searchView.isHidden = false
                       self.searchImage.isHidden = false
                   }else{
                       self.toast("No students for this phone number")
//                       self.searchView.isHidden = true
//                       self.searchImage.isHidden = true
                   }
                   self.studentsTableView.reloadData()
               }
           }
       }
}
