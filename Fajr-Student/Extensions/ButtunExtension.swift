//
//  ButtunExtension.swift
//  Fajr-Teacher
//
//  Created by osx on 12/15/20.
//  Copyright © 2020 M7mdAwad. All rights reserved.
//

import Foundation
import UIKit
class buttonWithBadge: UIButton {
    let badgeSize: CGFloat = 18
    let badgeTag = 9830384

    func badgeLabel(withCount count: Int) -> UILabel {
           let badgeCount = UILabel(frame: CGRect(x: 0, y: 0, width: badgeSize, height: badgeSize))
           badgeCount.translatesAutoresizingMaskIntoConstraints = false
           badgeCount.tag = badgeTag
           badgeCount.layer.cornerRadius = badgeCount.bounds.size.height / 2
           badgeCount.textAlignment = .center
           badgeCount.layer.masksToBounds = true
           badgeCount.textColor = .white
           badgeCount.font = badgeCount.font.withSize(10)
           badgeCount.backgroundColor = .systemRed
           badgeCount.text = String(count)
           return badgeCount
       }
       func showBadge(withCount count: Int) {
           
           let badge = badgeLabel(withCount: count)
           self.addSubview(badge)

           NSLayoutConstraint.activate([
               badge.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 12),
               badge.topAnchor.constraint(equalTo: self.topAnchor, constant: -8),
               badge.widthAnchor.constraint(equalToConstant: badgeSize),
               badge.heightAnchor.constraint(equalToConstant: badgeSize)
           ])
       }
}
