//
//  Extensions.swift
//  Sun Fun
//
//  Created by arab devolpers on 7/17/19.
//  Copyright © 2019 arab devolpers. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import FTToastIndicator
import FTNotificationIndicator
import DropDown
import ImageSlideshow
import SideMenu
extension UIViewController {
    
    func myNumbers()->CharacterSet{
    return  CharacterSet(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
    }
    func  uppercaseLetters()->CharacterSet{
     return CharacterSet.uppercaseLetters
    }
    
    func  lowercaseLetters()->CharacterSet{
     return CharacterSet.lowercaseLetters
    }
    func checkPassword(_ pass:UITextField , _ c_pass:UITextField)->Bool{
        guard !pass.text!.isEmpty else {
            self.toast("Enter Password")
            return false
        }
        guard pass.text!.count >= 8 else {
            self.toast("Password must be at least 8 characters")
            return false
        }
        guard pass.text!.rangeOfCharacter(from: self.myNumbers()) != nil else{
            self.toast("Password must contains at least two numbers")
            return false
        }
        guard pass.text!.rangeOfCharacter(from: self.uppercaseLetters()) != nil else {
            self.toast("Password must have at Least two uppercase letters")
            return false
        }
        guard pass.text!.rangeOfCharacter(from: self.lowercaseLetters()) != nil else {
            self.toast("Password must have at Least two lowercase letters")
            return false
        }
        
        guard !c_pass.text!.isEmpty else {
            self.toast("Enter Confirm Password")
            return false
        }
        guard c_pass.text! == pass.text! else {
            self.toast("Confirm Password And Password Not The Same")
            return false
        }
        return true
    }
    
    
    func checkValidPhone(_ phone:UITextField)->Bool{
        guard !(phone.text!.isEmpty) else{
            self.toast("Enter Phone Number")
            return false}
        guard (phone.text!.count >= 9 && phone.text!.count <= 11 ) else {
            self.toast("Enter valid phone number")
            return false
        }
        guard CharacterSet(charactersIn: phone.text!).isSubset(of: CharacterSet.decimalDigits) else {
            self.toast("Phone number must be only numbers")
            return false
        }
        return true
    }
    
    func haveMakeReservation(){
        ishaveReservation = false
        self.navigationController?.pushViewController(UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "ReservationsVC"), animated: false)
    }
    
    func getDateFromString(_ oldFormate:String,_ oldDate:String,_ newFormate:String)->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.locale = Locale(identifier: "en_US")
        dateFormatterGet.dateFormat = oldFormate
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = Locale(identifier: "en_US")
        dateFormatterPrint.dateFormat = newFormate
        if let date = dateFormatterGet.date(from: oldDate) {
            return dateFormatterPrint.string(from: date)
        } else {
           return "no"
        }
    }
    
    func changeDays(by days: Int,to selectedDate:Date)->Date {
          return Calendar.current.date(byAdding: .day, value: days, to: selectedDate)!
       }
    func is_ar()-> String{        
        return Bundle.main.preferredLocalizations[0]
    }
    func login(){
        UserDefaults.standard.set(false, forKey: "is_login")
        self.present(UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "authRoot"), animated: false, completion: nil)
    }
    func is_login()-> Bool{
        return UserDefaults.standard.bool(forKey: "is_login")
    }
    func getLang()-> String{
        if Bundle.main.preferredLocalizations[0] == "ar"{
            return "ara"
        }else{
            return "eng"
        }
    }
    func to_ast(_ string:String){
           FTToastIndicator.setToastIndicatorStyle(.extraLight)
           FTToastIndicator.showToastMessage(string)
       }
    func toast(_ string:String){
        FTToastIndicator.setToastIndicatorStyle(.prominent)
        FTToastIndicator.showToastMessage(string.localized())
    }
    func configSliderShow(_ slideView:ImageSlideshow,_ SlideImages:[String],_ img_url:String) {
           slideView.slideshowInterval = 5.0
           slideView.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
           slideView.contentScaleMode = UIView.ContentMode.scaleToFill
           let pageControl = UIPageControl()
           pageControl.currentPageIndicatorTintColor = UIColor(named: "MainColor")
           pageControl.pageIndicatorTintColor = UIColor.white
           slideView.pageIndicator = pageControl
           slideView.activityIndicator = DefaultActivityIndicator()
           slideView.activityIndicator = DefaultActivityIndicator(style: .gray , color: nil )
           slideView.addSubview(pageControl)
           slideShow(slideView,SlideImages,img_url)
       }
    func slideShow(_ slideView:ImageSlideshow,_ slideImages:[String],_ image_url:String) {
        var imgSource = [InputSource]()
        for item in slideImages{
            imgSource.append(SDWebImageSource(urlString:  item)!)
        }
        slideView.setImageInputs(imgSource)
    }

    func drop(anchor:UIView,dataSource:[String],completion: @escaping(_ item:String,_ index:Int)->Void) {
               let dropDown = DropDown()
               dropDown.anchorView = anchor
               dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)! + 5)
               dropDown.anchorView?.plainView.semanticContentAttribute = .unspecified
               dropDown.textColor = .black
               dropDown.dataSource = dataSource
               dropDown.textFont = UIFont(name: "neoSansArabic", size: 17.0)!
               dropDown.cellHeight = 40.0
               dropDown.layer.borderWidth = 1.0
               dropDown.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
               dropDown.clipsToBounds = true
               dropDown.selectionAction = {(index: Int, item: String) in
                   completion(item,index)
               }
               dropDown.show()
           }
     func rateApp(appId: String) {
         openUrl("itms-apps://itunes.apple.com/app/" + appId)
         }
         func openUrl(_ urlString:String) {
         let url = URL(string: urlString)!
         if #available(iOS 10.0, *) {
         UIApplication.shared.open(url, options: [:], completionHandler: nil)
         } else {
         UIApplication.shared.openURL(url)
         }
         }
    func ftt(_ txt:String){
        
        FTToastIndicator.setToastIndicatorStyle(.dark)
        FTToastIndicator.showToastMessage(txt.localized())
    }
    
    func openWhatsApp(_ phone_number:String){
        
                  let appURL = URL(string: "https://wa.me/\(phone_number)")!
                  if UIApplication.shared.canOpenURL(appURL) {
                      if #available(iOS 10.0, *) {
                          UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                      } else {
                          UIApplication.shared.openURL(appURL)
                      }
                  }
           
    }
    func showNoti(_ txt:String){
        FTNotificationIndicator.setNotificationIndicatorStyle(.dark)
        FTNotificationIndicator.showNotification(with: UIImage(named: "logo"), title: "خدمات", message: txt)
        FTNotificationIndicator.setDefaultDismissTime(4.0)
    }
    func showNoti_without_Localization(_ txt:String){
           FTNotificationIndicator.setNotificationIndicatorStyle(.dark)
           FTNotificationIndicator.showNotification(with: UIImage(named: "logo"), title: "خدمات", message: txt)
           FTNotificationIndicator.setDefaultDismissTime(4.0)
       }
    
    func ArabicNumReplacement(TF:UITextField,SS:String)->Bool {
           if TF.keyboardType == .numberPad && SS != "" {
               let numberStr: String = SS
               let formatter: NumberFormatter = NumberFormatter()
               formatter.locale = Locale(identifier: "EN")
               if let final = formatter.number(from: numberStr) {
                   TF.text =  "\(TF.text ?? "")\(final)"
               }
               return false
           }
           return true
       }
   
    func alertdone(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "ok".localized(), style: .default, handler: { action in
            _ = self.navigationController?.popToRootViewController(animated: true)
        })
       
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func sessionExpired() {
        let alertController = UIAlertController(title: "PPer".localized(), message: "Your session has expired".localized(), preferredStyle: .alert)
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 20.0)!]
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Cairo", size: 18.0)!]
         let titleAttrString = NSMutableAttributedString(string: "PPer".localized(), attributes: titleFont)
         let messageAttrString = NSMutableAttributedString(string: "Your session has expired".localized(), attributes: messageFont)
         alertController.setValue(titleAttrString, forKey: "attributedTitle")
         alertController.setValue(messageAttrString, forKey: "attributedMessage")
        let OKAction = UIAlertAction(title: "OK".localized(), style: .default, handler: { action in
            let vc = UIStoryboard(name: "Auth", bundle: nil)
              let rootVc = vc.instantiateViewController(withIdentifier: "authRoot")
              self.present(rootVc, animated: true, completion: nil)        })
        alertController.addAction(OKAction)
        alertController.view.tintColor = UIColor(named: "primaryColor")
        UserDefaults.standard.set(false, forKey: "is_login")
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func loginAlert() {
        let alertController = UIAlertController(title: "PPer Student".localized(), message: "loginPlease".localized(), preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "ok".localized(), style: .default, handler: { action in
             let vc = UIStoryboard(name: "Auth", bundle: nil)
               let rootVc = vc.instantiateViewController(withIdentifier: "authRoot")
               self.present(rootVc, animated: true, completion: nil)
        })
        alertController.addAction(OKAction)
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))

        self.present(alertController, animated: true, completion: nil)
    }
    
    func addgroupFunc(){
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Cairo-SemiBold", size: 20.0)!]
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Cairo", size: 18.0)!]
        let titleAttrString = NSMutableAttributedString(string: "\("PPer".localized()) \n", attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: "Would you like to add a group?!".localized(), attributes: messageFont)
        alertController.setValue(titleAttrString, forKey: "attributedTitle")
        alertController.setValue(messageAttrString, forKey: "attributedMessage")
        let OKAction = UIAlertAction(title: "OK".localized(), style: .default, handler: { action in
            self.navigationController?.pushViewController(UIStoryboard(name: "Groups", bundle: nil).instantiateViewController(withIdentifier: "AddGroupVC"), animated: false)
        })
        alertController.addAction(OKAction)
        alertController.addAction(UIAlertAction(title: "CANCEL".localized(), style: .cancel, handler: nil))

        self.present(alertController, animated: true, completion: nil)
    }

}
extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
    
}
class helperHelper:NSObject{
class func sideMenu(_ storyboard:String ,_ identifier:String)->SideMenuNavigationController{
        let menu = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier:identifier) as! SideMenuNavigationController
        menu.menuWidth = UIScreen.main.bounds.width/2 + UIScreen.main.bounds.width/4
        menu.presentationStyle = .menuSlideIn
    if Bundle.main.preferredLocalizations[0] == "ar" {menu.leftSide = false}else{menu.leftSide = true}
        return menu
    }
}
