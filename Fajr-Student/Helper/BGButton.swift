//
//  BGButton.swift
//  BGSetup
//
//  Created by Basem Elgendy on 2/23/19.
//  Copyright © 2019 Basem Elgendy. All rights reserved.
//

import UIKit
@IBDesignable
class BGButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    func selected(bgColor: UIColor , textColor: UIColor){
        self.backgroundColor = bgColor
        self.setTitleColor(textColor, for: .normal)
    }

}
