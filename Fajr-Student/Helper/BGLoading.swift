//
//  Loading.swift
//  
//
//  Created by Basem El-Gendy on 3/4/18.
//  Copyright © 2018 Basem El-Gendy. All rights reserved.
//

import UIKit
import JHSpinner

class BGLoading: NSObject {
    static let instance = BGLoading()
    static var spinner:JHSpinnerView?
    func dismissLoading(){
        if BGLoading.spinner != nil{
            BGLoading.spinner?.dismiss()
        }
    }
    func showLoading(_ view:UIView){
        if BGLoading.spinner != nil{
            BGLoading.spinner?.dismiss()
        }
        BGLoading.spinner = JHSpinnerView.showOnView(view, spinnerColor: UIColor.init(hexString: "ECAD3C"), overlay: .fullScreen, overlayColor: UIColor.black.withAlphaComponent(0.6), fullCycleTime: 4.0, text: "loading".localized(), textColor: UIColor.white)
        
        view.addSubview(BGLoading.spinner!)
    }
    func updateProgressLoadingView(progress:Double){
        BGLoading.spinner?.addCircleBorder(UIColor.init(hexString: "ECAD3C"), progress: CGFloat(progress / 100))
    }
    func showLoadingWithProgress(_ view:UIView){
        if BGLoading.spinner != nil{
            BGLoading.spinner?.dismiss()
        }
        BGLoading.spinner = JHSpinnerView.showOnView(view, spinnerColor: UIColor.init(hexString: "ECAD3C"), overlay: .fullScreen, overlayColor: UIColor.black.withAlphaComponent(0.6), fullCycleTime: 4.0, text: "loading".localized() , textColor: UIColor.white)
        
        view.addSubview(BGLoading.spinner!)
    }
}


