//
//  BGView.swift
//  BGSetup
//
//  Created by Basem Elgendy on 2/23/19.
//  Copyright © 2019 Basem Elgendy. All rights reserved.
//

import UIKit
@IBDesignable
class BGView: UIView {

    
}
extension UIView{

   
    
    //Allow to add shadow of any View from story board attributes
    @IBInspectable
    var haveShadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    
    @IBInspectable
    var percentageCornerRadius: CGFloat {
        set {
            
            let radius = (newValue * (UIScreen.main.bounds.height - self.frame.height)) / 2
            
            self.layer.cornerRadius = radius
        }
        get {
            return layer.cornerRadius
        }
    }
    
}
