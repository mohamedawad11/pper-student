
import Foundation
import UIKit
extension UICollectionView{
    func setCollectionLayOut(_ height:Int,_ count:Int){
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        layout.minimumInteritemSpacing = 5
        let xx = CGFloat(count * 10)
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - xx)/CGFloat(count), height: CGFloat(height))
        layout.scrollDirection = .horizontal
        self.collectionViewLayout  = layout
    }
    
    func setCollectionWithOutHorizontalLayOut(_ height:Int,_ count:Int){
          let layout = UICollectionViewFlowLayout()
          layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
          layout.minimumInteritemSpacing = 5
        let xx = CGFloat(count * 10)
        layout.itemSize = CGSize(width: (self.size.width - xx)/CGFloat(count), height: CGFloat(height))
         // layout.scrollDirection = .horizontal
          self.collectionViewLayout  = layout
      }
    
}
