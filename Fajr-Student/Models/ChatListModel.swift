
import Foundation

// MARK: - ChatListModel
struct ChatListModel: Codable {
    var collection: Collection?
    var teacher: Teacher?

    enum CodingKeys: String, CodingKey {
        case collection = "Collection"
        case teacher = "Teacher"
    }
}

// MARK: - Collection
struct Collection: Codable {
    var collectionName: String?
    var id: String?

    enum CodingKeys: String, CodingKey {
        case collectionName = "collection_name"
        case id = "id"
    }
}

// MARK: - Teacher
struct Teacher: Codable {
    var id: String?
    var lName: String?
    var fName: String?
    var mName: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case lName = "l_name"
        case fName = "f_name"
        case mName = "m_name"
    }
}
