// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let teachers = try? newJSONDecoder().decode(Teachers.self, from: jsonData)

import Foundation

// MARK: - Teachers
struct Teachers: Codable {
    var the0: The0?
    var member: Member?

    enum CodingKeys: String, CodingKey {
        case the0 = "0"
        case member = "Member"
    }
}

// MARK: - Member
struct Member: Codable {
    var id: String?
    var fName: String?
    var mName: String?
    var lName: String?
    var lon: String?
    var lat: String?
    var photo: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case fName = "f_name"
        case mName = "m_name"
        case lName = "l_name"
        case lon = "lon"
        case lat = "lat"
        case photo = "photo"
    }
}

// MARK: - The0
struct The0: Codable {
    var distance: String?

    enum CodingKeys: String, CodingKey {
        case distance = "distance"
    }
}
