
import Foundation

// MARK: - StudentModel
struct StudentModel: Codable {
    let id: String?
    let fName: String?
    let lName: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case fName = "f_name"
        case lName = "l_name"
    }
}

// MARK: - ReservationModel
struct ReservationModel: Codable {
    let id: String?
    let teacherSubjectID: String?
    let fawryRefrenceNumber: String?
    let invoiceID: String?
    let lat: String?
    let lon: String?
    let cost: String?
    let payType: String?
    let paid: String?
    let isRated: Bool?
    let belongsToMe: Bool?
    let upcoming: Bool?
    let willPay: Int?
    let collectionName: String?
    let teacher: String?
    let scheduleDay: String?
    let scheduleFrom: String?
    let scheduleTo: String?
    let status: String?
    let teacherAcceptReservation: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case teacherSubjectID = "teacher_subject_id"
        case fawryRefrenceNumber = "fawry_refrence_number"
        case invoiceID = "invoice_id"
        case lat = "lat"
        case lon = "lon"
        case cost = "cost"
        case payType = "pay_type"
        case paid = "paid"
        case isRated = "is_rated"
        case belongsToMe = "belongs_to_me"
        case upcoming = "upcoming"
        case willPay = "will_pay"
        case collectionName = "collectionName"
        case teacher = "teacher"
        case scheduleDay = "schedule_day"
        case scheduleFrom = "schedule_from"
        case scheduleTo = "schedule_to"
        case status = "status"
        case teacherAcceptReservation = "teacher_accept_reservation"
    }
}
