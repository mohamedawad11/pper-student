// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let reviewsModel = try? newJSONDecoder().decode(ReviewsModel.self, from: jsonData)

import Foundation

// MARK: - ReviewsModel
struct ReviewsModel: Codable {
    var review: Review?
    var student: Student?

    enum CodingKeys: String, CodingKey {
        case review = "Review"
        case student = "Student"
    }
}

// MARK: - Review
struct Review: Codable {
    var id: String?
    var studentID: String?
    var rate: String?
    var reviewBody: String?
    var teachersSubjectsID: String?
    var active: String?
    var created: String?
    var modified: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case studentID = "student_id"
        case rate = "rate"
        case reviewBody = "review_body"
        case teachersSubjectsID = "teachers_subjects_id"
        case active = "active"
        case created = "created"
        case modified = "modified"
    }
}

// MARK: - Student
struct Student: Codable {
    var id: String?
    var fName: String?
    var lName: String?
    var photo: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case fName = "f_name"
        case lName = "l_name"
        case photo = "photo"
    }
}
