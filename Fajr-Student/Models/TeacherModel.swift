
import Foundation

// MARK: - TeacherProfileModel
struct TeacherProfileModel: Codable {
    var id: String?
    var fName: String?
    var mName: String?
    var lName: String?
    var photo: String?
    var datOfBirth: String?
    var gender: String?
    var lat: String?
    var lon: String?
    var email: String?
    var phone: String?
    var street: String?
    var deviceToken: String?
    var accountNum: String?
    var resumeBody: String?
    var profileRatio: String?
    var lang: String?
    var subjectID: String?
    var costPerStudent: String?
    var deurationPerHoures: String?
    var countryName: String?
    var cityName: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case fName = "f_name"
        case mName = "m_name"
        case lName = "l_name"
        case photo = "photo"
        case datOfBirth = "dat_of_birth"
        case gender = "gender"
        case lat = "lat"
        case lon = "lon"
        case email = "email"
        case phone = "phone"
        case street = "street"
        case deviceToken = "device_token"
        case accountNum = "account_num"
        case resumeBody = "resume_body"
        case profileRatio = "profile_ratio"
        case lang = "lang"
        case subjectID = "subject_id"
        case costPerStudent = "cost_per_student"
        case deurationPerHoures = "deuration_per_houres"
        case countryName = "country_name"
        case cityName = "city_name"
    }
}
