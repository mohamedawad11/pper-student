//
//  ChatFromCells.swift
//  Fajr-Student
//
//  Created by osx on 1/5/21.
//

import UIKit

class ChatFromCells: UITableViewCell {
    @IBOutlet weak var message_lb: UILabel!
    @IBOutlet weak var username_lb: UILabel!
    @IBOutlet weak var date_lb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
