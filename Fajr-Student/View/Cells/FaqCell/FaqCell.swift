

import UIKit

class FaqCell: UITableViewCell {
    @IBOutlet weak var contentLB: UILabel!
    @IBOutlet weak var faqView: UIView!
    @IBOutlet weak var showBTN: UIButton!
    @IBOutlet weak var titleLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
