

import UIKit
class PreviousReservCell: UITableViewCell {
    @IBOutlet weak var collectionNameLB: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var teacherNameLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        if Bundle.main.preferredLocalizations[0] == "ar"{
            img.image = UIImage(named: "ff")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
