
import UIKit
import Cosmos
class ReviewCell: UITableViewCell {
    @IBOutlet weak var logo: UIImageView!
    
    @IBOutlet weak var dateLB: UILabel!
    @IBOutlet weak var commentLB: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var nameLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
