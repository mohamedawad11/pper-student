

import UIKit

class TransactionCell: UITableViewCell {
    @IBOutlet weak var bgv: BGView!
    @IBOutlet weak var teachernameLB: UILabel!
    @IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var scheduleDayLB: UILabel!
    @IBOutlet weak var createdLB: UILabel!
    @IBOutlet weak var amountLB: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
