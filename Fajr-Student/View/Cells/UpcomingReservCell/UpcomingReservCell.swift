
import UIKit

class UpcomingReservCell: UICollectionViewCell {
    @IBOutlet weak var collectionNameLb: UILabel!
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var teacherLb: UILabel!
    @IBOutlet weak var timeLb: UILabel!
    @IBOutlet weak var priceLb: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
